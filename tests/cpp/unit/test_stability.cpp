#include <cassert>
#include "dartsflash/stability/stability.hpp"
#include "dartsflash/flash/flash.hpp"
#include "dartsflash/eos/helmholtz/cubic.hpp"
#include "dartsflash/eos/aq/aq.hpp"
#include "dartsflash/global/global.hpp"
#include "dartsflash/eos/initial_guess.hpp"

int test_stability_vapour_liquid();
int test_stability_vapour_liquid_water();
int test_stability_vapour_brine();
int test_stationary_points_vapour_liquid();

struct Reference
{
	double pressure, temperature, tol{ 1e-5 };
	std::vector<double> X, tpd_ref;

	Reference(const double p, const double T, const std::vector<double>& x, const std::vector<double>& tpd) 
	: pressure(p), temperature(T), X(x), tpd_ref(tpd) {}

	int test(Stability *stability, FlashParams& flash_params, bool verbose)
	{
		if (verbose) { print_ref(); }
		flash_params.init_eos(pressure, temperature);
		flash_params.initial_guess.init(pressure, temperature);

		// Find reference compositions - hypothetical single phase
		std::vector<TrialPhase> ref_comps;
    	double gmin = NAN;
    	for (auto it: flash_params.eos_map)
    	{
        	if (it.second->eos_in_range(X.begin()))
        	{
            	double g = it.second->Gr_TP(pressure, temperature, X);
            	if (std::isnan(gmin) || g < gmin)
	            {
    	            ref_comps = {TrialPhase(it.first, X)};
        	        gmin = g;
            	}
	        }
    	}
		assert((!std::isnan(gmin)) && "No reference EoS found, gmin is NAN\n");
		
		if (verbose)
		{
			ref_comps[0].print_point("\nRef comp");
		}

		stability->init(ref_comps[0]);

		std::vector<double> tpd;
		for (auto it: flash_params.eos_params)
		{
			std::vector<TrialPhase> trial_comps = flash_params.initial_guess.evaluate(it.first, it.second.initial_guesses, ref_comps);
			for (TrialPhase trial_comp: trial_comps)
			{
				if (verbose)
				{
					trial_comp.print_point("\nTrialPhase");
				}

				int error = stability->run(trial_comp);
				if (error > 0)
				{
					print("Error in Stability", error);
					return error;
				}
			
				// Output and compare results
				tpd.push_back(trial_comp.tpd);
				if (verbose)
				{
					std::cout << "\nResults:\n";
					print("tpd", trial_comp.tpd);
					print("x", trial_comp.Y);
					std::cout << " Number of stability iterations ssi = " << stability->get_ssi_iter() << std::endl;
					std::cout << " Number of stability iterations newton = " << stability->get_newton_iter() << std::endl;
					std::cout << " Number of stability total iterations = " << stability->get_ssi_iter() + stability->get_newton_iter() << std::endl;
				}
			}
		}

		if (tpd.size() != tpd_ref.size())
		{
			print_ref();
			std::cout << "tpd and tpd_ref are not the same size\n";
			print("tpd", tpd);
			print("tpd_ref", tpd_ref);
			return 1;
		}
		for (size_t j = 0; j < tpd_ref.size(); j++)
		{
			// If one is nan, other is not
			if ((std::isnan(tpd_ref[j]) && !std::isnan(tpd[j])) || 
				(!std::isnan(tpd_ref[j]) && std::isnan(tpd[j])))
			{
				print_ref();
				std::cout << "Different values for tpd\n";
				print("tpd", tpd);
				print("tpd_ref", tpd_ref);
				return 1;
			}

			// For small tpd difference (tpd < 1), compare absolute difference; for large tpd values, logarithmic scale is used to compare
			double tpd_diff = std::log(tpd_ref[j]) < 0. ? std::fabs(tpd[j]-tpd_ref[j]) : std::log(tpd_ref[j]) - std::log(std::fabs(tpd[j]) + 1e-15);
			if (tpd_diff > tol)
			{
				print_ref();
				std::cout << "Different values for tpd\n";
				print("tpd", tpd);
				print("tpd_ref", tpd_ref);
				print("diff", tpd_diff);
				return 1;
			}
		}
		if (verbose)
		{
			std::cout << "Output is correct!\n";
		}
		
		// Check if matrix inverse is correct
		if (stability->test_matrices())
		{
			return 1;
		}
		return 0;
	}

	void print_ref()
	{
		std::cout << "==================================\n";
		print("p, T", std::vector<double>{pressure, temperature});
		print("X", X);
	}
};

int main()
{
	int error_output = 0;

	error_output += test_stability_vapour_liquid();
	error_output += test_stability_vapour_liquid_water();
	error_output += test_stability_vapour_brine();
	error_output += test_stationary_points_vapour_liquid();

    return error_output;
}

int test_stability_vapour_liquid()
{
	// Test stationary points for Y8 mixture with PR
	bool verbose = false;
	std::cout << (verbose ? "TESTING STATIONARY POINTS FOR Y8\n" : "");
    int error_output = 0;

	std::vector<std::string> comp{"C1", "C2", "C3", "C4", "C5", "C6"};
	std::vector<double> x = {0.8097, 0.0566, 0.0306, 0.0457, 0.0330, 0.0244};

	CompData comp_data(comp);
	comp_data.Pc = {45.99, 48.72, 42.48, 33.70, 27.40, 21.10};
	comp_data.Tc = {190.56, 305.32, 369.83, 469.70, 540.20, 617.70};
	comp_data.ac = {0.011, 0.099, 0.152, 0.252, 0.350, 0.490};
	comp_data.kij = std::vector<double>(6*6, 0.);

	FlashParams flash_params(comp_data);
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson};
	flash_params.eos_params["PR"].stability_switch_tol = 1e-1;

	std::vector<Reference> references = {
		Reference(220., 335., x, {-0.0010297, 0.}),
		Reference(10., 210., x, {-168482.8197, 0.}),
	};

	std::vector<bool> modcholesky = {false, true};
	std::vector<FlashParams::StabilityVars> vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_stab = modchol;
	
		for (FlashParams::StabilityVars var: vars)
		{
			flash_params.stability_variables = var;
			Stability stability(flash_params);

			for (Reference condition: references)
			{
				error_output += condition.test(&stability, flash_params, verbose);
			}
		}
	}

	// Test stationary points for Maljamar separator mixture (Orr, 1981), data from (Li, 2012) with PR
	std::cout << (verbose ? "TESTING STATIONARY POINTS FOR MALJAMAR SEPARATOR MIXTURE\n" : "");
	comp = {"CO2", "C5-7", "C8-10", "C11-14", "C15-20", "C21-28", "C29+"};
	int nc = 7;

    comp_data = CompData(comp);
    comp_data.Pc = {73.9, 28.8, 23.7, 18.6, 14.8, 12.0, 8.5};
    comp_data.Tc = {304.2, 516.7, 590.0, 668.6, 745.8, 812.7, 914.9};
    comp_data.ac = {0.225, 0.265, 0.364, 0.499, 0.661, 0.877, 1.279};
    comp_data.kij = std::vector<double>(7*7, 0.);
    comp_data.set_binary_coefficients(0, {0.0, 0.115, 0.115, 0.115, 0.115, 0.115, 0.115});

	std::vector<double> z_init = {0.0, 0.2354, 0.3295, 0.1713, 0.1099, 0.0574, 0.0965};

	flash_params = FlashParams(comp_data);
	flash_params.stability_variables = FlashParams::alpha;
	// flash_params.verbose = true;

	pr = CubicEoS(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
    flash_params.eos_params["PR"].stability_switch_tol = 1e-1;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson,
													 InitialGuess::Yi::Wilson13,
													 0};  // pure CO2 initial guess
	
	references = {
		Reference(64., 305.35, z_init, {0., -0.003385820305, 0., 0.01355258769, -0.003385820305}),
		Reference(68.5, 305.35, z_init, {-0.2834475754, -0.02790311075, -0.2834475754, -0.02790311075, -0.02790311075})
	};

	std::vector<double> zCO2 = {0.65, 0.9};
	for (size_t j = 0; j < zCO2.size(); j++)
	{
		references[j].X[0] = zCO2[j];
    	for (int i = 1; i < nc; i++)
    	{
        	references[j].X[i] = z_init[i]*(1.0-zCO2[j]);
    	}
	}

	for (bool modchol: modcholesky)
	{
		flash_params.modChol_stab = modchol;
	
		for (FlashParams::StabilityVars var: vars)
		{
			flash_params.stability_variables = var;
			Stability stability(flash_params);

			for (Reference condition: references)
			{
				error_output += condition.test(&stability, flash_params, verbose);
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_stability_vapour_liquid()", error_output);
	}
	else
	{
		print("No errors occurred in test_stability_vapour_liquid()", error_output);
	}
    return error_output;
}

int test_stability_vapour_liquid_water()
{
	// Test stationary points for NWE OIL mixture with PR
	bool verbose = false;
	std::cout << (verbose ? "TESTING STATIONARY POINTS FOR NWE OIL\n" : "");
    int error_output = 0;
	std::vector<std::string> comp = {"H2O","CO2", "C1", "C2-3", "C4-6", "C7-14", "C15-24", "C25+"};
	CompData comp_data = CompData(comp);
	comp_data.Pc = {220.48, 73.76, 46.0, 45.05, 33.50,24.24, 18.03, 17.26};
	comp_data.Tc = {647.3, 304.2, 190.6, 343.64, 466.41, 603.07, 733.79, 923.2};
	comp_data.ac = {0.344, 0.225, 0.008, 0.130, 0.244, 0.6, 0.903, 1.229};
	comp_data.kij = std::vector<double>(8*8, 0.);
	comp_data.set_binary_coefficients(0, {0., 0.1896, 0.4850, 0.5, 0.5, 0.5, 0.5, 0.5});
	comp_data.set_binary_coefficients(1, {0.1896, 0., 0.12, 0.12, 0.12, 0.09, 0.09, 0.09});

	std::vector<double> x = {0.5, 0.251925, 0.050625, 0.02950, 0.0371, 0.071575, 0.03725, 0.022025};

	FlashParams flash_params(comp_data);
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson};
	flash_params.eos_params["PR"].stability_switch_tol = 1e-1;

	std::vector<Reference> references = {
		Reference(400., 600., x, {-0.04434285365,-0.08876706415}),
	};

	std::vector<bool> modcholesky = {false, true};
	std::vector<FlashParams::StabilityVars> vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_stab = modchol;
	
		for (FlashParams::StabilityVars var: vars)
		{
			flash_params.stability_variables = var;
			Stability stability(flash_params);

			for (Reference condition: references)
			{
				error_output += condition.test(&stability, flash_params, verbose);
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_stability_vapour_liquid_water()", error_output);
	}
	else
	{
		print("No errors occurred in test_stability_vapour_liquid_water()", error_output);
	}
    return error_output;
}

int test_stability_vapour_brine()
{
	// Test stationary points for Brine-Vapour mixture with PR and AQ
	bool verbose = false;
	std::cout << (verbose ? "TESTING STATIONARY POINTS FOR MIXED EOS\n" : "");
	int error_output = 0;

	// Test ternary mixture H2O-CO2-C1
	std::vector<std::string> comp = {"H2O", "CO2", "C1"};
	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	FlashParams flash_params(comp_data);
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].initial_guesses = {0, 1};
	flash_params.eos_params["PR"].stability_switch_tol = 1e-2;
	flash_params.eos_params["PR"].stability_max_iter = 10;

	std::map<AQEoS::CompType, AQEoS::Model> aq_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
    AQEoS aq(comp_data, aq_map);
	aq.set_eos_range(0, std::vector<double>{0.9, 1.});
	flash_params.add_eos("AQ", &aq);
	flash_params.eos_params["AQ"].initial_guesses = {0};
	flash_params.eos_params["AQ"].stability_switch_tol = 1e-10;
	flash_params.eos_params["AQ"].stability_max_iter = 10;
	flash_params.preferred_eos[0] = "AQ";

	std::vector<double> min_z = {1e-10, 1e-5};
	std::vector<Reference> references = {
		Reference(1., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.705034989, 0., 0.}),
		Reference(1., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.013848965, 0.9982570595, 0.}),
		Reference(1., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {0.1223598444, 0., 0.951172582 }),
		Reference(1., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {-0.04966906725, 0., -4307.116395}),
		Reference(1., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.506545159, 0., 0.}),
		Reference(1., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.051711748, 0.999128895, 0.}),
		Reference(1., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.218922906, 0., 0.}),
		Reference(1., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0.9991860566, 0.9991257638, 0.}),
		Reference(1., 273.15, {0.5, 0.25, 0.25}, {-74.85585513, -85.39210409, 0.}),
		// Reference(1., 273.15, {0.9, 0.05, 0.05}, {1.163763166e-05, -0.02203224993, -1146.424093}),
		Reference(10., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.871554332, 0., 0.}),
		Reference(10., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.016950304, 0.9840079825, 0.}),
		Reference(10., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {0.1213388576, 0., 0.9949039905}),
		Reference(10., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {-0.05063856789, 0., -447.3166644}),
		Reference(10., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.644240342, 0., 0.}),
		Reference(10., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.086445029, 0.9922494722, 0.}),
		Reference(10., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.309930453, 0., 0.}),
		Reference(10., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0.9922332763, 0.9916704529, 0.}),
		Reference(10., 273.15, {0.5, 0.25, 0.25}, {-9.098643618, -10.42586476, -76.6001877 }),
		// Reference(10., 273.15, {0.9, 0.05, 0.05}, {1.163763166e-05, -0.02084490363, -118.6829405}),
		Reference(100., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {2.025872379, 0., 0.}),
		Reference(100., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.008131321, 0.9523660844, 0.}),
		Reference(100., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {0.1109482284, 0., 0.}),
		Reference(100., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {-0.06018433748, 0., -64.38221581}),
		Reference(100., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.813947603, 0., 0.}),
		Reference(100., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.225725529, 0., 0.}),
		Reference(100., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.408624256, 0., 0.}),
		Reference(100., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0.9524680145, 0.9499223439, 0.}),
		Reference(100., 273.15, {0.5, 0.25, 0.25}, {-8.929040345, -10.09862637, -11.26490457}),
		// Reference(100., 273.15, {0.9, 0.05, 0.05}, {1.163763166e-05, -0.00891623654, -16.88946701}),
		Reference(1., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.838188517, 0., 0.}),
		Reference(1., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.103157324, 0., 0.}),
		Reference(1., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {0.04364610018, 0., 0.}),
		Reference(1., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {0.04365566248, 0., 0.}),
		Reference(1., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.529866297, 0., 0.}),
		Reference(1., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.110237842, 0., 0.}),
		Reference(1., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.321462424, 0., 0.}),
		Reference(1., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.016450148, 0., 0.}),
		Reference(1., 373.15, {0.5, 0.25, 0.25}, {0.5211784592, 0., 0.}),
		// Reference(1., 373.15, {0.9, 0.05, 0.05}, {-74.85585513, 0., 0.}),
		Reference(10., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {2.010633174, 0., 0. }),
		Reference(10., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.176826766, 0., 0. }),
		Reference(10., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {0.01036359275, 0., 0.}),
		Reference(10., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {0.009990545369, 0., -1.220248879}),
		Reference(10., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.663053651, 0., 0.}),
		Reference(10., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.186042075, 0., 0.}),
		Reference(10., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.423252496, 0., 0.}),
		Reference(10., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.037601597, 0., 0.}),
		Reference(10., 373.15, {0.5, 0.25, 0.25}, {-3.45673736, -3.501427851, 0. }),
		// Reference(10., 373.15, {0.9, 0.05, 0.05}, {-74.85585513, 0., 0.}),
		Reference(100., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {2.178261078, 0., 0.}),
		Reference(100., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.26991424, 0., 0.}),
		Reference(100., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {0., 0.0005391942896, 0.0005391942896}),
		Reference(100., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {0., 0.0005486672476, 0.0005486672476}),
		Reference(100., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.806612814, 0., 0.}),
		Reference(100., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.27905683, 0., 0.}),
		Reference(100., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.53750584, 0., 0.}),
		Reference(100., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.073693281, 0., 0.}),
		Reference(100., 373.15, {0.5, 0.25, 0.25}, {-4.395689722, -4.382115602, -1.316298063 }),
		// Reference(100., 373.15, {0.9, 0.05, 0.05}, {-74.85585513, 0., 0.}),
	};

	std::vector<bool> modcholesky = {false, true};
	std::vector<FlashParams::StabilityVars> vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_stab = modchol;
	
		for (FlashParams::StabilityVars var: vars)
		{
			flash_params.stability_variables = var;
			Stability stability(flash_params);

			for (Reference condition: references)
			{
				error_output += condition.test(&stability, flash_params, verbose);
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_stability_vapour_brine()", error_output);
	}
	else
	{
		print("No errors occurred in test_stability_vapour_brine()", error_output);
	}
	return error_output;
}

int test_stationary_points_vapour_liquid()
{
	// Test stationary points on Y8 mixture
	const bool verbose = false;
	std::cout << (verbose ? "TESTING STATIONARY POINTS FOR Y8\n" : "");
	int error_output = 0;
	double tol = 1e-7;

	std::vector<std::string> comp{"C1", "C2", "C3", "nC5", "nC7", "nC10"};
	std::vector<double> x = {0.8097, 0.0566, 0.0306, 0.0457, 0.0330, 0.0244};

	CompData comp_data(comp);
	comp_data.Pc = {45.99, 48.72, 42.48, 33.70, 27.40, 21.10};
	comp_data.Tc = {190.56, 305.32, 369.83, 469.70, 540.20, 617.70};
	comp_data.ac = {0.011, 0.099, 0.152, 0.252, 0.350, 0.490};
	comp_data.kij = std::vector<double>(6*6, 0.);

	FlashParams flash_params(comp_data);
	flash_params.stability_variables = FlashParams::alpha;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson, InitialGuess::Yi::Wilson13, 0, 1, 2, 3, 4, 5};
	flash_params.eos_params["PR"].stability_switch_tol = 1e-2;

	Analysis analysis(flash_params);
	std::vector<std::string> eos{"PR"};

	std::vector<double> pres = {220., };
	std::vector<double> temp = {335., };
	std::vector<std::vector<double>> tpd_ref{{-0.0010297, 0.}, };

	std::vector<bool>modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_stab = modchol;
		for (size_t j = 0; j < pres.size(); j++)
		{
			std::vector<TrialPhase> stationary_points = analysis.find_stationary_points(pres[j], temp[j], x, eos);
			if (stationary_points.size() != tpd_ref[j].size())
			{
				print("tpd_ref", tpd_ref[j]);
				for (TrialPhase sp: stationary_points)
				{
					sp.print_point();
				}
				error_output++;
			}
			else
			{
				for (TrialPhase sp: stationary_points)
				{
					if (verbose) { sp.print_point(); }
					if (std::find_if(tpd_ref[j].begin(), tpd_ref[j].end(), [&sp, tol](double ref) { return std::fabs(sp.tpd - ref) < tol; }) == tpd_ref[j].end())
					{
						error_output++;
					}
				}
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_stationary_points_vapour_liquid()", error_output);
	}
	else
	{
		print("No errors occurred in test_stationary_points_vapour_liquid()", error_output);
	}
	return error_output;
}
