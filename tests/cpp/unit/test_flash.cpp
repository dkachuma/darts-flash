#include "dartsflash/flash/flash.hpp"
#include "dartsflash/stability/stability.hpp"
#include "dartsflash/eos/helmholtz/cubic.hpp"
#include "dartsflash/eos/aq/jager.hpp"
#include "dartsflash/eos/aq/ziabakhsh.hpp"
#include "dartsflash/eos/vdwp/ballard.hpp"
#include "dartsflash/eos/solid/solid.hpp"
#include "dartsflash/global/global.hpp"

int test_negativeflash2_vapour_liquid();
int test_stabilityflash2_vapour_liquid_3comp();
int test_stabilityflash2_vapour_liquid();
int test_stabilityflashN_vapour_liquid();
int test_stabilityflashN_vapour_liquid_water();

int test_negativeflash2_vapour_brine();
int test_stabilityflash2_vapour_brine();
int test_negativeflash2_vapour_brine_ions();
int test_stabilityflashN_vapour_brine_si();
int test_stabilityflashN_vapour_brine_ice();

struct Reference
{
	double pressure, temperature, nu_tol{ 1e-5 };
	std::vector<double> composition, nu_ref;

	Reference(const double p, const double T, const std::vector<double>& z, const std::vector<double>& nu) 
	: pressure(p), temperature(T), composition(z), nu_ref(nu) {}

	int test(Flash *flash, bool verbose)
	{
		if (verbose)
		{
			std::cout << "==================================\n";
			print("p, T", std::vector<double>{pressure, temperature});
			print("z", composition);
		}
		int error = flash->evaluate(pressure, temperature, composition);
		if (error > 0)
		{
			print("Error in Flash", error);
			print("p, T", std::vector<double>{pressure, temperature});
			print("z", composition);
			return error;
		}
		
		// Output and compare results
		Flash::Results flash_results = flash->get_flash_results();
		if (verbose)
		{
			std::cout << "\nResults:\n";
			print("nu", flash_results.nu);
			print("x", flash_results.X, static_cast<int>(flash_results.nu.size()));
			std::cout << " Number of flash iterations ssi = " << flash->get_flash_total_ssi_iter() << std::endl;
			std::cout << " Number of flash iterations newton = " << flash->get_flash_total_newton_iter() << std::endl;
			std::cout << " Number of flash total iterations = " << flash->get_flash_total_ssi_iter() + flash->get_flash_total_newton_iter() << std::endl;
		}

		if (flash_results.nu.size() != nu_ref.size())
		{
			std::cout << "nu and nu_ref are not the same size\n";
			print("p, T", std::vector<double>{pressure, temperature});
			print("z", composition);
			print("nu", flash_results.nu);
			print("nu_ref", nu_ref);
			return 1;
		}
		if (this->compare_nu(flash_results.nu))
		{
			std::cout << "Different values for nu\n";
			print("p, T", std::vector<double>{pressure, temperature});
			print("z", composition);
			print("nu", flash_results.nu);
			print("nu_ref", nu_ref);
			return 1;
		}
		if (verbose)
		{
			std::cout << "Output is correct!\n";
		}
		return 0;
	}

	int compare_nu(std::vector<double>& nu)
	{
		if (nu_ref.size() == 2)
		{
			if (std::sqrt(std::pow(nu_ref[0]-nu[0], 2)) > nu_tol)
			{
				if (std::sqrt(std::pow(nu_ref[1]-nu[0], 2)) > nu_tol)
				{
					return 1;
				}
			}
		}
		else
		{
			for (size_t j = 0; j < nu_ref.size(); j++)
			{
				if (std::sqrt(std::pow(nu_ref[j]-nu[j], 2)) > nu_tol)
				{
					return 1;
				}
			}
		}
		return 0;
	}
};

int main()
{
	int error_output = 0;
	
	error_output += test_negativeflash2_vapour_liquid();
	error_output += test_stabilityflash2_vapour_liquid_3comp();
	error_output += test_stabilityflash2_vapour_liquid();
	error_output += test_stabilityflashN_vapour_liquid();
	// error_output += test_stabilityflashN_vapour_liquid_water();

	error_output += test_negativeflash2_vapour_brine();
	error_output += test_stabilityflash2_vapour_brine();
	error_output += test_negativeflash2_vapour_brine_ions();
	error_output += test_stabilityflashN_vapour_brine_si();
	error_output += test_stabilityflashN_vapour_brine_ice();

    return error_output;
}

int test_negativeflash2_vapour_liquid()
{
	// Test negative flash on Y8 mixture
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P NEGATIVEFLASH VAPOUR-LIQUID\n" : "");
    int error_output = 0;

	std::vector<std::string> comp{"C1", "C2", "C3", "nC5", "nC7", "nC10"};
	CompData comp_data(comp);
	comp_data.Pc = {45.99, 48.72, 42.48, 33.70, 27.40, 21.10};
	comp_data.Tc = {190.56, 305.32, 369.83, 469.70, 540.20, 617.70};
	comp_data.ac = {0.011, 0.099, 0.152, 0.252, 0.350, 0.490};
	comp_data.kij = std::vector<double>(6*6, 0.);

	std::vector<double> z = {0.8097, 0.0566, 0.0306, 0.0457, 0.0330, 0.0244};

	CubicEoS pr(comp_data, CubicEoS::PR);

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.split_max_iter = 50;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);

	std::vector<Reference> references = {
		Reference(1.01325, 298.15, z, {0.9610356705, 0.0389643295}),
		Reference(100., 335., z, {0.8544397779, 0.1455602221}),
		Reference(220., 335., z, {0.8886759694, 0.1113240306}),
		Reference(10., 210., z, {0.8280558586, 0.1719441414 })
	};

	std::vector<FlashParams::SplitVars> vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	for (FlashParams::SplitVars var: vars)
	{
		flash_params.split_variables = var;
		NegativeFlash flash(flash_params, {"PR", "PR"}, {InitialGuess::Ki::Wilson_VL});
		for (Reference condition: references)
		{
			error_output += condition.test(&flash, verbose);
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_negativeflash2_vapour_liquid()", error_output);
	}
	else
	{
		print("No errors occurred in test_negativeflash2_vapour_liquid()", error_output);
	}
    return error_output;
}

int test_stabilityflash2_vapour_liquid_3comp()
{
	// Test stability flash for a 3-component mixture
	// It can be used to quickly verify new implemebtations like Newton modified mole number
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P STABILITYFLASH VAPOUR-LIQUID\n" : "");
    int error_output = 0;

	//Test 1 ->H2O-C4-C20 mixture
	std::vector<std::string> comp{"H2O", "C4", "C20"};
	CompData comp_data(comp);
	comp_data.Pc = {220.5, 38., 14.6};
	comp_data.Tc = {647., 425.2, 782.};
	comp_data.ac = {0.344, 0.1928, 0.8160};
	comp_data.kij = std::vector<double>(3*3, 0.);
	comp_data.set_binary_coefficients(0, {0., 0.5, 0.5, 0.5, 0.5});

	std::vector<double> z = {0.8, 0.16, 0.04};

	CubicEoS pr(comp_data, CubicEoS::PR);

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.split_max_iter = 50;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].stability_switch_tol = 1e-3;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson};

	std::vector<Reference> references = {
		Reference(50, 500, z, {0.947413562, 0.0525865263}),
	};

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik};
	for (FlashParams::StabilityVars stab_var: stab_vars)
	{
		flash_params.stability_variables = stab_var;
		for (FlashParams::SplitVars split_var: split_vars)
		{
			flash_params.split_variables = split_var;
			StabilityFlash flash(flash_params, 2);
			for (Reference condition: references)
			{
				error_output += condition.test(&flash, verbose);
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflash2_vapour_liquid()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflash2_vapour_liquid()", error_output);
	}
    return error_output;
}

int test_stabilityflash2_vapour_liquid()
{
	// Test stability flash
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P STABILITYFLASH VAPOUR-LIQUID\n" : "");
    int error_output = 0;

	//Test 1 ->Y8 mixture
	std::vector<std::string> comp{"C1", "C2", "C3", "nC5", "nC7", "nC10"};
	CompData comp_data(comp);
	comp_data.Pc = {45.99, 48.72, 42.48, 33.70, 27.40, 21.10};
	comp_data.Tc = {190.56, 305.32, 369.83, 469.70, 540.20, 617.70};
	comp_data.ac = {0.011, 0.099, 0.152, 0.252, 0.350, 0.490};
	comp_data.kij = std::vector<double>(6*6, 0.);

	std::vector<double> z = {0.8097, 0.0566, 0.0306, 0.0457, 0.0330, 0.0244};

	CubicEoS pr(comp_data, CubicEoS::PR);

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-1;
	flash_params.split_max_iter = 50;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].stability_switch_tol = 1e-1;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson};

	std::vector<Reference> references = {
		Reference(1.01325, 298.15, z, {0.9610356705, 0.0389643295}),
		Reference(100., 335., z, {0.1455602221, 0.8544397779}),
		Reference(220., 335., z, {0.8886759694, 0.1113240306}),
		Reference(250., 200., z, {1.}),
		Reference(16., 235., z, {0.8408949466, 0.1591050534}),
		Reference(220., 350., z, {0.9640546469, 0.03594535311}),
		Reference(1., 150., z, {0.1878472774, 0.8121527226}),
		Reference(191.,275., z, {0.622366, 0.377634}),
	};

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::StabilityVars stab_var: stab_vars)
		{
			flash_params.stability_variables = stab_var;
			for (FlashParams::SplitVars split_var: split_vars)
			{
				flash_params.split_variables = split_var;
				StabilityFlash flash(flash_params, 2);
				for (Reference condition: references)
				{
					error_output += condition.test(&flash, verbose);
				}
			}
		}
	}
	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflash2_vapour_liquid()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflash2_vapour_liquid()", error_output);
	}
    return error_output;
}

int test_stabilityflashN_vapour_liquid()
{
	// Test Maljamar separator mixture (Orr, 1981), data from (Li, 2012)
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH\n" : "");
	int error_output = 0;

    int np_max = 3;
    std::vector<std::string> comp = {"CO2", "C5-7", "C8-10", "C11-14", "C15-20", "C21-28", "C29+"};
	int nc = 7;

    CompData comp_data(comp);
    comp_data.Pc = {73.9, 28.8, 23.7, 18.6, 14.8, 12.0, 8.5};
    comp_data.Tc = {304.2, 516.7, 590.0, 668.6, 745.8, 812.7, 914.9};
    comp_data.ac = {0.225, 0.265, 0.364, 0.499, 0.661, 0.877, 1.279};
    comp_data.kij = std::vector<double>(7*7, 0.);
    comp_data.set_binary_coefficients(0, {0.0, 0.115, 0.115, 0.115, 0.115, 0.115, 0.115});

	std::vector<double> z_init = {0.0, 0.2354, 0.3295, 0.1713, 0.1099, 0.0574, 0.0965};

	FlashParams flash_params(comp_data);
    flash_params.split_switch_tol = 1e-3;
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
    flash_params.eos_params["PR"].stability_switch_tol = 1e-1;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson, 
													 InitialGuess::Yi::Wilson13,
													 0};  // pure CO2 initial guess

	std::vector<std::pair<Reference, double>> references = {
		// {Reference(64., 305.35, z_init, {0.98716891, 0.01283109005}), 0.65},
		// {Reference(64., 305.35, z_init, {0.98716891, 0.01283109005}), 0.68},
		{Reference(68.5, 305.35, z_init, {0.2742159703, 0.5881748853, 0.1376091443}), 0.90},
		{Reference(68., 305.35, z_init, {0.9278943981, 0.05395067044, 0.01815493149 }), 0.70},
	};

	for (size_t j = 0; j < references.size(); j++)
	{
		references[j].first.composition[0] = references[j].second;
    	for (int i = 1; i < nc; i++)
    	{
        	references[j].first.composition[i] = z_init[i]*(1.0-references[j].second);
    	}
	}

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};

	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 

		for (FlashParams::StabilityVars stab_var: stab_vars)
		{
			flash_params.stability_variables = stab_var;
			for (FlashParams::SplitVars split_var: split_vars)
			{
				flash_params.split_variables = split_var;
				StabilityFlash flash(flash_params, np_max);
				for (std::pair<Reference, double> condition: references)
				{
					error_output += condition.first.test(&flash, verbose);
				}
			}
		}
	}

	
	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflashN_vapour_liquid()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflashN_vapour_liquid()", error_output);
	}
    return error_output;
}

int test_stabilityflashN_vapour_liquid_water()
{
	// Test Water/NWE mixture (Khan et al, 1992), data from (Li, 2018)
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH\n" : "");
	int error_output = 0;

    int np_max = 3;

	std::vector<std::string> comp = {"H2O","CO2", "C1", "C2-3", "C4-6", "C7-14", "C15-24", "C25+"};
	CompData comp_data = CompData(comp);
	comp_data.Pc = {220.48, 73.76, 46.0, 45.05, 33.50,24.24, 18.03, 17.26};
	comp_data.Tc = {647.3, 304.20, 190.6, 343.64, 466.41, 603.07, 733.79, 923.2};
	comp_data.ac = {0.344, 0.225, 0.008, 0.13, 0.244, 0.6, 0.903, 1.229};
	comp_data.kij = std::vector<double>(8*8, 0.);
	comp_data.set_binary_coefficients(0, {0., 0.1896, 0.4850, 0.5, 0.5, 0.5, 0.5, 0.5});
	comp_data.set_binary_coefficients(1, {0.1896, 0., 0.12, 0.12, 0.12, 0.09, 0.09, 0.09});

	std::vector<double> z = {0.5, 0.251925, 0.050625, 0.02950, 0.0371, 0.071575, 0.03725, 0.022025};

	FlashParams flash_params(comp_data);
    flash_params.split_switch_tol = 1e-3;
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
    flash_params.eos_params["PR"].stability_switch_tol = 1e-3;
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Wilson,
													 InitialGuess::Yi::Wilson13};  // pure H2O initial guess

	std::vector<Reference> references = {
		Reference(200., 500., z, {0.4046631329, 0.36088833279, 0.23444853426}),
	};

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::StabilityVars stab_var: stab_vars)
		{
			flash_params.stability_variables = stab_var;
			for (FlashParams::SplitVars split_var: split_vars)
			{
				flash_params.split_variables = split_var;
				StabilityFlash flash(flash_params, np_max);
				for (Reference condition: references)
				{
					error_output += condition.test(&flash, verbose);
				}
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflashN_vapour_liquid_water()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflashN_vapour_liquid_water()", error_output);
	}
    return error_output;
}

int test_negativeflash2_vapour_brine()
{
	//
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P NEGATIVEFLASH VAPOUR-BRINE\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};
	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	CubicEoS pr(comp_data, CubicEoS::PR);

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.9, 1.});

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.split_tol = 1e-10;
	flash_params.rr2_tol = 1e-15;
	flash_params.min_z = 1e-13;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	flash_params.add_eos("AQ", &aq);

	std::vector<std::vector<double>> zero = {{1.-1e-11, 1e-11, 0.}, {1e-11, 1.-1e-11, 0.}};
	std::vector<double> min_z = {1e-10, 1e-5};
	std::vector<Reference> references = {
		// Freezing point
		Reference(1., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		Reference(1., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(1., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(1., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		Reference(1., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		Reference(1., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(1., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(1., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		Reference(1., 273.15, {0.5, 0.25, 0.25}, {0.4970505533, 0.5029494467}),
		Reference(1., 273.15, {0.8, 0.1, 0.1}, {0.7992302375, 0.2007697625}),
		Reference(1., 273.15, {0.9, 0.05, 0.05}, {0.8999551701, 0.1000448299}),
		Reference(1., 273.15, zero[0], {1., 0.}),
		Reference(1., 273.15, zero[1], {0., 1.}),
		Reference(10., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		Reference(10., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(10., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(10., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		Reference(10., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		Reference(10., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(10., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(10., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		Reference(10., 273.15, {0.5, 0.25, 0.25}, {0.5028281496, 0.4971718504}),
		Reference(10., 273.15, {0.8, 0.1, 0.1}, {0.8048675531, 0.1951324469}),
		Reference(10., 273.15, {0.9, 0.05, 0.05}, {0.9054062135, 0.0945937865}),
		Reference(10., 273.15, zero[0], {1., 0.}),
		Reference(10., 273.15, zero[1], {0., 1.}),
		Reference(100., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		Reference(100., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(100., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(100., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		Reference(100., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		Reference(100., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(100., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(100., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		Reference(100., 273.15, {0.5, 0.25, 0.25}, {0.5132407229, 0.4867592771}),
		Reference(100., 273.15, {0.8, 0.1, 0.1}, {0.8204201149, 0.1795798851}),
		Reference(100., 273.15, {0.9, 0.05, 0.05}, {0.9209840974, 0.07901590259}),
		Reference(100., 273.15, zero[0], {1., 0.}),
		Reference(100., 273.15, zero[1], {0., 1.}),

		// Boiling point
		// Reference(1., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.838212638, 0., 0.}),
		// Reference(1., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.103157324, 0., 0.}),
		Reference(1., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(1., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		// Reference(1., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		// Reference(1., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		// Reference(1., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		// Reference(1., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		// Reference(1., 373.15, {0.5, 0.25, 0.25}, {0.5210220846, 0., 0.}),
		// Reference(1., 373.15, {0.8, 0.1, 0.1}, {0.1795798851, 0.8204201149}),
		// Reference(1., 373.15, {0.9, 0.05, 0.05}, {-74.85585513, 0., 0.}),
		// Reference(1., 373.15, zero[0], {0.1000448299, 0.8999551701}),
		// Reference(1., 373.15, zero[1], {0.1000448299, 0.8999551701}),
		Reference(10., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		Reference(10., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(10., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(10., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		// Reference(10., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		// Reference(10., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(10., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(10., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		// Reference(10., 373.15, {0.5, 0.25, 0.25}, {0.439293082, 0.560706918}),
		// Reference(10., 373.15, {0.8, 0.1, 0.1}, {0.7763746233, 0.2236253767}),
		// Reference(10., 373.15, {0.9, 0.05, 0.05}, {0.8887319884, 0.1112680116}),
		// Reference(10., 373.15, zero[0], {1., 0.}),
		// Reference(10., 373.15, zero[1], {0., 1.}),
		// Reference(100., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {0., 1.}),
		// Reference(100., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {0., 1.}),
		Reference(100., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1., 0.}),
		Reference(100., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1., 0.}),
		Reference(100., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {0., 1.}),
		Reference(100., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {0., 1.}),
		Reference(100., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {0., 1.}),
		Reference(100., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {0., 1.}),
		// Reference(100., 373.15, {0.5, 0.25, 0.25}, {0.4958719596, 0.5041280404}),
		// Reference(100., 373.15, {0.8, 0.1, 0.1}, {0.8031386567, 0.1968613433}),
		// Reference(100., 373.15, {0.9, 0.05, 0.05}, {0.9053836364, 0.09461636357}),
		// Reference(100., 373.15, zero[0], {1., 0.}),
		// Reference(100., 373.15, zero[1], {0., 1.}),

		// Difficult conditions
		// Reference(109.29, 300.1, {0.999, 0.001, 0.}, {1., 0.}), // difficult conditions
		Reference(30., 330., {0.6, 0.39, 0.01}, {0.6019105675, 0.3980894325}) // difficult conditions
	};

	// Test conventional (y/x) phase ordering
	std::cout << (verbose ? "CALCULATING IN ORDER: AQ-V\n" : "");
	std::vector<FlashParams::SplitVars> vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::SplitVars var: vars)
		{
			flash_params.split_variables = var;
			NegativeFlash flash(flash_params, {"AQ", "PR"}, {InitialGuess::Ki::Henry_AV});
			for (Reference condition: references)
			{
				error_output += condition.test(&flash, verbose);
			}
		}
	}

	// Test reverse phase ordering
	std::cout << (verbose ? "CALCULATING IN ORDER: V-AQ\n" : "");
	for (FlashParams::SplitVars var: vars)
	{
		flash_params.split_variables = var;
		NegativeFlash flash(flash_params, {"AQ", "PR"}, {InitialGuess::Ki::Henry_AV});
		for (Reference condition: references)
		{
			error_output += condition.test(&flash, verbose);
		}
	}

	for (Reference condition: references)
	{
		condition.nu_ref = std::vector<double>{condition.nu_ref[1], condition.nu_ref[0]};
		for (FlashParams::SplitVars var: vars)
		{
			flash_params.split_variables = var;
			NegativeFlash flash(flash_params, {"PR", "AQ"}, {InitialGuess::Ki::Henry_VA});
			error_output += condition.test(&flash, verbose);
		}
	}
	
	if (error_output > 0)
	{
		print("Errors occurred in test_negativeflash2_vapour_brine()", error_output);
	}
	else
	{
		print("No errors occurred in test_negativeflash2_vapour_brine()", error_output);
	}
	return error_output;
}

int test_stabilityflash2_vapour_brine()
{
	//
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P STABILITYFLASH VAPOUR-BRINE\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};
	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	FlashParams flash_params(comp_data);
	flash_params.split_switch_tol = 1e-3;
	flash_params.rr2_tol = 1e-15;
	flash_params.min_z = 1e-13;
	flash_params.verbose = verbose;

	CubicEoS pr(comp_data, CubicEoS::PR);
	flash_params.add_eos("PR", &pr);
	flash_params.eos_params["PR"].initial_guesses = {InitialGuess::Yi::Henry,
									 				 InitialGuess::Yi::Wilson,
													 1, 2};
	flash_params.eos_params["PR"].stability_switch_tol = 1e-2;
	flash_params.eos_params["PR"].stability_max_iter = 10;

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.9, 1.});
	flash_params.add_eos("AQ", &aq);
	flash_params.eos_params["AQ"].initial_guesses = {//InitialGuess::Yi::Henry,
									 				 0};
	flash_params.eos_params["AQ"].stability_max_iter = 10;
	flash_params.preferred_eos[0] = "AQ";
	
	std::vector<std::vector<double>> zero = {{1.-1e-11, 1e-11, 0.}, {1e-11, 1.-1e-11, 0.}};
	std::vector<double> min_z = {1e-10, 1e-5};
	std::vector<Reference> references = {
		// Freezing point
		Reference(1., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(1., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(1., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(1., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(1., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(1., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(1., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(1., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(1., 273.15, {0.5, 0.25, 0.25}, {0.4970505533, 0.5029494467}),
		Reference(1., 273.15, {0.8, 0.1, 0.1}, {0.7992302375, 0.2007697625}),
		// Reference(1., 273.15, {0.9, 0.05, 0.05}, {0.8999551701, 0.1000448299}),
		Reference(1., 273.15, zero[0], {1.}),
		Reference(1., 273.15, zero[1], {1.}),
		Reference(10., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(10., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(10., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(10., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(10., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(10., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(10., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(10., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(10., 273.15, {0.5, 0.25, 0.25}, {0.5028281496, 0.4971718504}),
		Reference(10., 273.15, {0.8, 0.1, 0.1}, {0.8048675531, 0.1951324469}),
		// Reference(10., 273.15, {0.9, 0.05, 0.05}, {0.9054062135, 0.0945937865}),
		Reference(10., 273.15, zero[0], {1.}),
		Reference(10., 273.15, zero[1], {1.}),
		Reference(100., 273.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(100., 273.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(100., 273.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(100., 273.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(100., 273.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(100., 273.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(100., 273.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(100., 273.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(100., 273.15, {0.5, 0.25, 0.25}, {0.5132407229, 0.4867592771}),
		Reference(100., 273.15, {0.8, 0.1, 0.1}, {0.8204201149, 0.1795798851}),
		// Reference(100., 273.15, {0.9, 0.05, 0.05}, {0.9209840974, 0.07901590259}),
		Reference(100., 273.15, zero[0], {1.}),
		Reference(100., 273.15, zero[1], {1.}),

		// Boiling point
		// Reference(1., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.838212638, 0., 0.}),
		// Reference(1., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.103157324, 0., 0.}),
		// Reference(1., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		// Reference(1., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		// Reference(1., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		// Reference(1., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		// Reference(1., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		// Reference(1., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		// Reference(1., 373.15, {0.5, 0.25, 0.25}, {0.5210220846, 0., 0.}),
		// Reference(1., 373.15, {0.8, 0.1, 0.1}, {0.1795798851, 0.8204201149}),
		// Reference(1., 373.15, {0.9, 0.05, 0.05}, {-74.85585513, 0., 0.}),
		// Reference(1., 373.15, zero[0], {0.1000448299, 0.8999551701}),
		// Reference(1., 373.15, zero[1], {0.1000448299, 0.8999551701}),
		Reference(10., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(10., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(10., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(10., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(10., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(10., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(10., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(10., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(10., 373.15, {0.5, 0.25, 0.25}, {0.5607295931, 0.4392704069}),
		Reference(10., 373.15, {0.8, 0.1, 0.1}, {0.7763746233, 0.2236253767}),
		// Reference(10., 373.15, {0.9, 0.05, 0.05}, {0.8887319884, 0.1112680116}),
		Reference(10., 373.15, zero[0], {1.}),
		Reference(10., 373.15, zero[1], {1.}),
		Reference(100., 373.15, {min_z[0], 0.5, 0.5-min_z[0]}, {1.}),
		Reference(100., 373.15, {min_z[1], 0.5, 0.5-min_z[1]}, {1.}),
		Reference(100., 373.15, {1.-min_z[0], 0.5*min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(100., 373.15, {1.-min_z[1], 0.5*min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(100., 373.15, {0.5*min_z[0], 1.-min_z[0], 0.5*min_z[0]}, {1.}),
		Reference(100., 373.15, {0.5*min_z[1], 1.-min_z[1], 0.5*min_z[1]}, {1.}),
		Reference(100., 373.15, {0.5*min_z[0], 0.5*min_z[0], 1.-min_z[0]}, {1.}),
		Reference(100., 373.15, {0.5*min_z[1], 0.5*min_z[1], 1.-min_z[1]}, {1.}),
		Reference(100., 373.15, {0.5, 0.25, 0.25}, {0.4958719596, 0.5041280404}),
		Reference(100., 373.15, {0.8, 0.1, 0.1}, {0.8031386567, 0.1968613433}),
		// Reference(100., 373.15, {0.9, 0.05, 0.05}, {0.9053836364, 0.09461636357}),
		Reference(100., 373.15, zero[0], {1.}),
		Reference(100., 373.15, zero[1], {1.}),

		// Difficult conditions
		Reference(109.29, 300.1, {0.999, 0.001, 0.}, {1.}), // difficult conditions
		Reference(30., 330., {0.6, 0.39, 0.01}, {0.6019105675, 0.3980894325}) // difficult conditions
	};

	// references = {
	// 	Reference(100., 273.15, {0.8, 0.1, 0.1}, {0.8204201149, 0.1795798851}),
	// };

	std::vector<FlashParams::StabilityVars> stab_vars = {FlashParams::Y, FlashParams::lnY, FlashParams::alpha};
	std::vector<FlashParams::SplitVars> split_vars = {FlashParams::nik, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::StabilityVars stab_var: stab_vars)
		{
			flash_params.stability_variables = stab_var;
			for (FlashParams::SplitVars split_var: split_vars)
			{
				flash_params.split_variables = split_var;
				StabilityFlash flash(flash_params, 2);
				for (Reference condition: references)
				{
					error_output += condition.test(&flash, verbose);
				}
			}
		}
	}

	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflash2_vapour_brine()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflash2_vapour_brine()", error_output);
	}
	return error_output;
}

int test_negativeflash2_vapour_brine_ions()
{
	//
	bool verbose = false;
	std::cout << (verbose ? "TESTING 2P NEGATIVEFLASH VAPOUR-BRINE WITH IONS\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};
	std::vector<std::string> ions = {"Na+", "Cl-"};

	CompData comp_data(comp, ions);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};
	comp_data.charge = {1, -1};

	CubicEoS pr(comp_data, CubicEoS::PR);

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);

	FlashParams flash_params(comp_data);
	flash_params.rr_max_iter = 10;
	flash_params.split_max_iter = 50;
	flash_params.verbose = verbose;

	flash_params.add_eos("PR", &pr);
	flash_params.add_eos("AQ", &aq);

	std::vector<std::vector<double>> Z = {{0.5, 0.25, 0.2, 0.025, 0.025}, {0.5, 5e-06, 0.449995, 0.025, 0.025}};
	std::vector<Reference> references = {
		Reference(1.01325, 298.15, Z[0], {0.536237103, 0.463762897}),
		Reference(1.01325, 298.15, Z[1], {0.5361915827, 0.4638084173}),
		Reference(100., 335., Z[0], {0.551499262, 0.448500738}),
		Reference(100., 335., Z[1], {0.5491050143, 0.4508949857}),
		Reference(54.5, 277.6, Z[0], {0.5558883338, 0.4441116662 }),
		Reference(54.5, 277.6, Z[1], {0.5502680968, 0.4497319032}),
	};

	std::vector<FlashParams::SplitVars> vars = {FlashParams::nik};//, FlashParams::lnK, FlashParams::lnK_chol};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (FlashParams::SplitVars var: vars)
		{
			flash_params.split_variables = var;
			NegativeFlash flash(flash_params, {"AQ", "PR"}, {InitialGuess::Ki::Henry_AV});
			for (Reference condition: references)
			{
				error_output += condition.test(&flash, verbose);
			}
		}
	}
	if (error_output > 0)
	{
		print("Errors occurred in test_negativeflash2_vapour_brine_ions()", error_output);
	}
	else
	{
		print("No errors occurred in test_negativeflash2_vapour_brine_ions()", error_output);
	}
	return error_output;
}

int test_stabilityflashN_vapour_brine_si()
{
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH VAPOUR-BRINE-sI\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};

	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	CubicEoS pr(comp_data, CubicEoS::PR);
	pr.set_eos_range(0, std::vector<double>{0., 0.9});

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);
	aq.set_eos_range(0, std::vector<double>{0.9, 1.});

	Ballard si(comp_data, "sI");

	FlashParams flash_params(comp_data);
	flash_params.add_eos("PR", &pr);
	flash_params.add_eos("AQ", &aq);
	flash_params.add_eos("sI", &si);

	flash_params.split_variables = FlashParams::nik;
	flash_params.split_switch_tol = 1e-3;
	flash_params.stability_variables = FlashParams::alpha;
	flash_params.verbose = verbose;

	std::vector<int> initial_guesses = {InitialGuess::Yi::Henry, 1, 2};

	StabilityFlash flash(flash_params, 2);
	std::vector<double> pres, temp, nu, x, nu_ref, x_ref;

	pres = {5., 10., 15.};
	std::vector<double> xH2O = {0.5, 0.86, 0.88, 0.9, 0.95};
	std::vector<double> xCO2 = {1e-3, 0.1, 0.5, 0.9, 1.-1e-3};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (double p: pres)
		{
			for (double X0: xH2O)
			{
				for (double X1: xCO2)
				{
					std::vector<double> z = {X0, (1.-X0)*X1, (1.-X0)*(1.-X1)};
					// print("z", z);
					error_output += flash.evaluate(p, 100, z);
					// nu = flash.get_phase_fractions();
					// x = flash.get_phase_compositions();
					// print("nu", nu);
					// print("x", x, static_cast<int>(nu.size()));
					// print("Error", error_output);
				}
			}
		}
	}
	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflashN_vapour_brine_si()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflashN_vapour_brine_si()", error_output);
	}
	return error_output;
}

int test_stabilityflashN_vapour_brine_ice()
{
	bool verbose = false;
	std::cout << (verbose ? "TESTING NP STABILITYFLASH VAPOUR-BRINE WITH ICE\n" : "");
	int error_output = 0;

	std::vector<std::string> comp = {"H2O", "CO2", "C1"};

	CompData comp_data(comp);
	comp_data.Pc = {220.50, 73.75, 46.04};
	comp_data.Tc = {647.14, 304.10, 190.58};
	comp_data.ac = {0.328, 0.239, 0.012};
	comp_data.kij = std::vector<double>(3*3, 0.);
    comp_data.set_binary_coefficients(0, {0., 0.19014, 0.47893});
	comp_data.set_binary_coefficients(1, {0.19014, 0., 0.0936});
	comp_data.Mw = {18.015, 44.01, 16.043};

	CubicEoS pr(comp_data, CubicEoS::PR);

	std::map<AQEoS::CompType, AQEoS::Model> evaluator_map = {
		{AQEoS::CompType::water, AQEoS::Model::Jager2003},
		{AQEoS::CompType::solute, AQEoS::Model::Ziabakhsh2012},
		{AQEoS::CompType::ion, AQEoS::Model::Jager2003}
	};
	AQEoS aq(comp_data, evaluator_map);

	PureSolid ice(comp_data, "Ice");

	FlashParams flash_params(comp_data);
	flash_params.add_eos("PR", &pr);
	flash_params.add_eos("AQ", &aq);
	flash_params.add_eos("Ih", &ice);

	flash_params.rr_max_iter = 10;
	flash_params.split_max_iter = 50;
	flash_params.split_variables = FlashParams::lnK;
	flash_params.verbose = verbose;

	StabilityFlash flash(flash_params, 3);
	std::vector<double> pres, temp, nu, x, nu_ref, x_ref;

	pres = {5., 10., 15.};
	temp = {253.15, 263.15, 273.15, 283.15, 298.15};
	std::vector<double> z = {0.8, 0.2-1e-3, 1e-3};
	std::vector<bool> modcholesky = {false, true};
	for (bool modchol: modcholesky)
	{
		flash_params.modChol_split = modchol;
		flash_params.modChol_stab = modchol; 
		for (double T: temp)
		{
			for (double p: pres)
			{
				// std::cout << "Calculating T = " << T << "; P = " << p << std::endl;
				error_output += flash.evaluate(p, T, z);
				Flash::Results flash_results = flash.get_flash_results();
				// print("nu", flash_results.nu);
				// print("x", flash_results.X, static_cast<int>(flash_results.nu.size()));
				// print("Error", error_output);
			}
		}
	}
	if (error_output > 0)
	{
		print("Errors occurred in test_stabilityflashN_vapour_brine_ice()", error_output);
	}
	else
	{
		print("No errors occurred in test_stabilityflashN_vapour_brine_ice()", error_output);
	}
	return error_output;
}
