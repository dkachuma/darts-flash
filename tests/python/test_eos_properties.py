import numpy as np
import xarray as xr
import pytest

import dartsflash.libflash
from dartsflash.libflash import CubicEoS, AQEoS, InitialGuess
from dartsflash.diagram.analysis import Analysis
from .conftest import mixture_brine_vapour, mixture_brine_vapour_ions


# Test of full compositional space for H2O-CO2-C1 mixture
@pytest.fixture()
def analysis_obj(mixture_brine_vapour):
    mix = mixture_brine_vapour
    a = Analysis(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])
    a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012}))
    a.init_flash(stabilityflash=False, eos=["AQ", "PR"], initial_guess=[InitialGuess.Henry_AV])
    return a


@pytest.fixture()
def ref_eos_properties():
    return "./tests/python/data/ref_eos_properties.nc"


def test_eos_properties(analysis_obj, ref_eos_properties):
    state_spec = ["pressure", "temperature"]
    zrange = np.concatenate([np.array([1e-12, 1e-10, 1e-8]),
                             np.linspace(1e-6, 1. - 1e-6, 10),
                             np.array([1. - 1e-8, 1. - 1e-10, 1. - 1e-12])])
    dimensions = {"pressure": np.array([1., 2., 5., 10., 50., 100., 200., 400.]),
                  "temperature": np.arange(273.15, 373.15, 10),
                  "H2O": zrange,
                  "CO2": zrange,
                  }
    constants = {"C1": 1.}
    dims_order = ["H2O",
                  "CO2",
                  "pressure",
                  "temperature"]
    properties = {"V": analysis_obj.eos["PR"].V,
                  "G": analysis_obj.eos["PR"].G_TP,
                  "H": analysis_obj.eos["PR"].H_TP,
                  "Cvr": analysis_obj.eos["PR"].Cvr,
                  "Cv": analysis_obj.eos["PR"].Cv,
                  "Cpr": analysis_obj.eos["PR"].Cpr,
                  "Cp": analysis_obj.eos["PR"].Cp,
                  "JT": analysis_obj.eos["PR"].JT,
                  "vs": analysis_obj.eos["PR"].vs,
                  }
    props = list(properties.keys())

    results = analysis_obj.evaluate_properties_1p(state_spec=state_spec, dimensions=dimensions, constants=constants,
                                                  properties_to_evaluate=properties, mole_fractions=True, dims_order=dims_order)
    results = results[props]

    if 1:
        # Use assertions from xarray
        ref = xr.open_dataset(ref_eos_properties)
        ref = ref[props]

        try:
            xr.testing.assert_allclose(results, ref, rtol=1e-5)
        except AssertionError:
            for i, p in enumerate(results.pressure.values):
                for j, t in enumerate(results.temperature.values):
                    for k, h2o in enumerate(results.H2O.values):
                        for m, co2 in enumerate(results.CO2.values):
                            v = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).V.values
                            v_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).V.values
                            g = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).G.values
                            g_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).G.values
                            h = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).H.values
                            h_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).H.values
                            jt = results.isel(pressure=i, temperature=j, H2O=k, CO2=m).JT.values
                            jt_ref = ref.isel(pressure=i, temperature=j, H2O=k, CO2=m).JT.values
                            if not np.allclose(v, v_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("v != v_ref", p, t, h2o, co2, v, v_ref)
                            elif not np.allclose(g, g_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("g != g_ref", p, t, h2o, co2, g, g_ref)
                            elif not np.allclose(h, h_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("h != h_ref", p, t, h2o, co2, h, h_ref)
                            elif not np.allclose(jt, jt_ref, rtol=1e-5, equal_nan=True):
                                raise AssertionError("jt != jt_ref", p, t, h2o, co2, jt, jt_ref)

    else:
        # Update reference file
        results.to_netcdf(ref_eos_properties)
