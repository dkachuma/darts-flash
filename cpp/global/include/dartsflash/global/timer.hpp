//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_GLOBAL_TIMER_H
#define OPENDARTS_FLASH_GLOBAL_TIMER_H
//--------------------------------------------------------------------------

#include <vector>
#include <string>
#include <chrono>

class Timer
{
private:
    std::chrono::time_point<std::chrono::steady_clock> t0, t1;
    double time;
    bool running = false;

public:
    Timer() { time = 0.; }
    ~Timer() = default;

    void start();
    void stop();
    
    double elapsedMicroseconds();
    double elapsedMilliseconds() { return this->elapsedMicroseconds() * 1e-3; }
    double elapsedSeconds() {return this->elapsedMicroseconds() * 1e-6; }
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_GLOBAL_TIMER_H
//--------------------------------------------------------------------------
