#include <vector>
#include <cmath>

#include "dartsflash/global/global.hpp"

bool compare_compositions(std::vector<double>& X0, std::vector<double>& X1, double tol)
{
    for (size_t i = 0; i < X0.size(); i++)
    {
        if (std::fabs(X0[i] - X1[i]) > tol) // composition is different
        {
            return false;
        }
    }
    // composition is the same
    return true;
}
