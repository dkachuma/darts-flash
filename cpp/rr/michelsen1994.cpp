#include <iostream>
#include <cmath>
#include <algorithm>
#include <numeric>

#include "dartsflash/rr/rr.hpp"
#include "dartsflash/global/global.hpp"
#include <Eigen/Dense>

Michelsen1994::Michelsen1994(FlashParams& flash_params, int nc_, int np_) : RR(flash_params, nc_, np_)
{
    this->K.reserve(np*nc);
}

int Michelsen1994::solve_rr(std::vector<double>& z_, std::vector<double>& K_, const std::vector<int>& nonzero_comp_) {
    // Minimization of MRR [Michelsen, 1994] formulated with K-values (fugacity coefficients in reference paper)
    std::vector<double> nu_new(np, 1./np);
    K = std::vector<double>(nc, 1.);
    K.insert(this->K.end(), K_.begin(), K_.end());
    this->init(z_, K, nonzero_comp_);

    // Calculate objective function
    double Q, Q_new = this->calc_obj(nu_new);
    if (Q_new != Q_new) { std::cout << "NaN value in Q!\n"; exit(1); }
    Eigen::MatrixXd H(np, np);
    Eigen::VectorXd dnu(np);
    g = Eigen::VectorXd(np);
    std::vector<bool> active(np);

    int iter = 1;
    while (iter < max_iter)
    {
        nu = nu_new;
        Q = Q_new;

        // set positive phases to active
        std::transform(nu.begin(), nu.end(), active.begin(), [](double nu_j) { return (nu_j > 0.); });

        // Calculate inverse of Ei for use in gradient vector and Hessian calculation
        std::vector<double> Ei_inv(nc);
        std::transform(Ei.begin(), Ei.end(), Ei_inv.begin(), [](double ei) { return 1./ei; });

        // Calculate gradient vector
        this->update_g(active, Ei_inv);

        // Calculate Hessian H = U z U^T
        for (int j = 0; j < np; j++)
        {
            for (int k = j; k < np; k++)
            {
                if (active[j] && active[k])
                {
                    double H_jk = 0.;
                    for (int i = 0; i < nc; i++)
                    {
                        H_jk += z[i] * std::pow(Ei_inv[i], 2) * K[j*nc + i] * K[k*nc + i];
                    }
                    H(j, k) = H_jk;
                    H(k, j) = H_jk;
                }
                else
                {
                    if (j == k)
                    {
                        H(j, j) = 1.;
                    }
                    else
                    {
                        H(j, k) = 0.;
                        H(k, j) = 0.;
                    }
                }
            }
        }
        
        // Calculate Newton step
        dnu = H.llt().solve(g);

        double lambda = 1.;
        int line_iter = 1;
        while (line_iter < max_iter)
        {
            // Check if all active phases are positive after Newton step
            // For this, find value for lambda such that exactly one B_new is 0 (minimum positive value of lambda)
            for (int j = 0; j < np; j++)
            {
                double a = nu[j]/dnu(j);
                if ((active[j]) && (0. < a && a < lambda))
                {
                    lambda = a;
                }
            }
            for (int j = 0; j < np; j++)
            {
                nu_new[j] = nu[j] - lambda*dnu(j);
            }

            Q_new = calc_obj(nu_new);

            if (Q_new < Q)
            {
                nu = nu_new;
                Q = Q_new;

                // Calculate norm of dnu
                if (dnu.squaredNorm() < rrn_tol)
                {
                    // Calculate gradient vector
                    std::vector<double> dQ_dnu(np, 1.);
                    for (int j = 0; j < np; j++)
                    {
                        for (int i = 0; i < nc; i++)
                        {
                            dQ_dnu[j] -= z[i] * K[j*nc + i] / Ei[i];
                        }
                    }

                    // If constraint dQ/dnu_m > 0 is satisfied for all absent phases, we have found the solution
                    // If not, we need to re-introduce a phase; the one with the most negative dQ/dnu_j
                    int idx = 0;
                    double min_dQdnu = 0.;
                    for (int j = 0; j < np; j++)
                    {
                        if (!active[j])
                        {
                            if (dQ_dnu[j] < min_dQdnu)
                            {
                                idx = j;
                                min_dQdnu = dQ_dnu[j];
                            }
                        }
                    }
                    // If there is a dQ/dnu_m < 0, re-introduce the absent phase with the most negative gradient
                    if (min_dQdnu < 0.)
                    {
                        nu_new[idx] = 1e-3;
                    }
                    else
                    {
                        return 0;
                    }
                }
                break;
            }
            else
            {
                lambda *= 0.5;
            }
            line_iter++;
        }
        iter++;
    }
    return 1;
}

double Michelsen1994::calc_obj(std::vector<double>& nu_) {
    // Calculate objective function and Ei
    Ei = std::vector<double>(nc, 0.);
    std::vector<double> lnE(nc);
    for (int i = 0; i < nc; i++)
    {
        for (int j = 0; j < np; j++)
        {
            Ei[i] += nu_[j] * K[j*nc + i];
        }
        lnE[i] = std::log(Ei[i]);
    }
    
    // Calculate updated objective function Q = Σ nu - Σ zi ln(Ei)
    double sum_nu = std::accumulate(nu_.begin(), nu_.end(), 0.);
    double sum_zlnE = std::inner_product(z.begin(), z.end(), lnE.begin(), 0.);
    return sum_nu - sum_zlnE;
}

void Michelsen1994::update_g(std::vector<bool>& active, std::vector<double>& Ei_inv)
{
    // Construct gradient vector
    for (int j = 0; j < np; j++)
    {
        if (active[j])
        {
            g(j) = 1.;
            for (int i = 0; i < nc; i++)
            {
                // For reference phase, K-values are equal to 1
                g(j) -= z[i] * K[j*nc + i] * Ei_inv[i];
            }
        }
        else
        {
            g(j) = 0.;
        }
    }
    return;
}

std::vector<double> Michelsen1994::getx() {
    // Calculation of x from Michelsen MRR
    std::vector<double> x(np*nc);

    for (int i = 0; i < nc; i++)
    {
        for (int j = 0; j < np; j++)
        {
            x[j*nc + i] = K[j*nc + i] * z[i] / Ei[i];
        }
    }

    return x;
}
