#include <iostream>
#include <cmath>
#include <algorithm>

#include "dartsflash/rr/rr.hpp"
#include "dartsflash/global/global.hpp"

using namespace std;

void RRN2::bounds() {   
    // Check whether solution to RR equation lies within [0, 1]
    // Calc objective function at b = 0
    this->gdg(0.);
    if (g < 0.)
    {
        // Since objective function is monotonically decreasing, 
        // we know that the solution is smaller than zero if g(0) < 0
        v_max = 0.;

        // The lower bound is the asymptote below zero
        double eps = 1e-15;
        double maxK = *std::max_element(K.begin(), K.end());
        v_min = 1. / (1. - maxK) + eps;
        return;
    }
    // Calculate objective function at b = 1
    this->gdg(1.);
    if (g > 0.)
    {
        // Likewise, we know that the solution is larger than 1 if g(1) > 0
        v_min = 1.;

        // The upper bound is the asymptote larger than 1
        double eps = 1e-15;
        double minK = *std::min_element(K.begin(), K.end());
        v_max = 1. / (1. - minK) - eps;
        return;
    }

    // Else, we know we are located within the interval [0, 1]
    v_min = 0.;
    v_max = 1.;
    return;
}

int RRN2::solve_rr(std::vector<double>& z_, std::vector<double>& K_, const std::vector<int>& nonzero_comp_) {
    // Solve two-phase RR equation
    this->init(z_, K_, nonzero_comp_);
    double b0, b1;

    // First, determine bounds of solution domain
    this->bounds();

    // Calculate first b0 with bisection
    b0 = this->bisection();
    this->update_bounds(b0);

    int iter = 0;
    while (true)
    {        
        // Perform Newton step
        // If step overshoots limits, it will do bisection instead
        b1 = this->newton(b0);
        this->update_bounds(b1);
        iter++;

        // Check if it has converged to abs(g) < tol || abs(db) < tol
        if (std::fabs(this->g) < rr2_tol)
        {
            nu = {1-b1, b1};
            return 0;
        }
        else if (std::fabs(b1-b0) < rr2_tol) {
            nu = {1-b1, b1};
            return 0;
        }
        else if (iter == max_iter)
        {
            nu = {1-b1, b1};
            return 1;
        }
        b0 = b1;
    }
}

void RRN2::gdg(double b0) {
    g = 0.;
    dg = 0.;
	for (int i = 0; i < nc; i++) 
	{
		double m_i = 1. + b0 * (K[i] - 1.);
		g += z[i] * (K[i] - 1.) / m_i; // f_j
		dg -= z[i] * (K[i] - 1.) / std::pow(m_i, 2.) * (K[i] - 1.); // df_j/dv_j
	}
    return;
}

void RRN2::update_bounds(double b) {
    this->gdg(b);
    if (g < 0)
    {
        v_max = b;
    }
    else
    {
        v_min = b;
    }
    return;
}

double RRN2::bisection() {
    // Perform bisection step
    return 0.5*(v_min + v_max);
}

double RRN2::newton(double b0) {
    // Newton update of b
    double db = g/dg;
    double b1 = b0 - db;
    
    // if within bounds of bmin and bmax, accept
    if (v_min < b1 && b1 < v_max)
    {
        return b1;
    }
    // else do bisection
    else
    {
        return this->bisection();
    }
}
