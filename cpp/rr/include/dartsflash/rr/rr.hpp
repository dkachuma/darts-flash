//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_RR_RR_H
#define OPENDARTS_FLASH_RR_RR_H
//--------------------------------------------------------------------------

#include "dartsflash/flash/flash_params.hpp"
#include <vector>
#include <Eigen/Dense>

class RR
{
protected:
    int np, nc;
    double norm;
	double min_z, rr2_tol, rrn_tol;
    int max_iter;
    std::vector<int> nonzero_comp;
    std::vector<double> z, K, nu;

public:
    RR(FlashParams& flash_params, int nc_, int np_);
    virtual ~RR() = default;

    virtual int solve_rr(std::vector<double>& z_, std::vector<double>& K_, const std::vector<int>& nonzero_comp_={}) = 0;
    
    const std::vector<double> &getnu() const { return this->nu; }
    virtual std::vector<double> getx();

protected:
    void init(std::vector<double>& z_, std::vector<double>& K_, const std::vector<int>& nonzero_comp_);
    std::vector<double> objective_function(const std::vector<double>& nu_);
    int output(int error);
};

// Regular 2-phase Rachford-Rice with negative flash
class RRN2 : public RR
{
private:
    double g, dg, v_min, v_mid, v_max;

public:
    RRN2(FlashParams& flash_params, int nc_) : RR(flash_params, nc_, 2) {}

    virtual int solve_rr(std::vector<double>& z_, std::vector<double>& K_, const std::vector<int>& nonzero_comp_={}) override;
    
private:
    void bounds();
    void update_bounds(double b0);
    void gdg(double b0);

    double bisection();
    double newton(double b0);

};

// 2-phase Rachford-Rice with convex transformations (Nichita, 2013)
class RRN2Convex : public RR
{
private:
    std::vector<double> ci, di;
    std::vector<int> k_idxs;

public:
    RRN2Convex(FlashParams& flash_params, int nc_);

    virtual int solve_rr(std::vector<double>& z_, std::vector<double>& K_, const std::vector<int>& nonzero_comp_={}) override;
    virtual std::vector<double> getx() override;

private:
    int solve_gh();
    int solve_fgh();
    int output(int error, double a);

    std::vector<int> sort_idxs(std::vector<double> ki);

    // Functions aL, aR, V, F(a), F'(a), G(a), G'(a), H(a) and H'(a)
    double aL(double z1);
    double aR(double zn);
    double V(double a);

    double F(double a);
    double dF(double a);
	double G(double a);
    double G(double a, double f);
    double dG(double a);
	double H(double a);
    double H(double a, double g);
	double dH(double a);
};

// N-phase Rachford-Rice with negative flash
class RRN : public RR 
{
protected:
    int error_output;
    std::vector<double> f;
    std::vector<double> v_min, v_mid, v_max;
    
public:
    RRN(FlashParams& flash_params, int nc_, int np_);
    
    virtual int solve_rr(std::vector<double>& z_, std::vector<double>& K_, const std::vector<int>& nonzero_comp_={}) override;

protected:
    int bounds(std::vector<double> V_j, int j);
    bool bounded();
    
    int rr_loop(int J);
    double fdf(std::vector<double> V_j, int J); // calculates f_j and df_j/dv_j
};

// N-phase Rachford-Rice (Michelsen, 1994)
class Michelsen1994 : public RR
{
protected:
    std::vector<double> Ei;
    Eigen::VectorXd g;

public:
    Michelsen1994(FlashParams& flash_params, int nc_, int np_);

    virtual int solve_rr(std::vector<double>& z_, std::vector<double>& K_, const std::vector<int>& nonzero_comp_={}) override;
    virtual std::vector<double> getx() override;

    // Calculate norm of gradient vector
    double l2norm() { return g.squaredNorm(); }

protected:
    virtual double calc_obj(std::vector<double>& nu_);
    void update_g(std::vector<bool>& active, std::vector<double>& Ei_inv);

};

class Yan2012 : public Michelsen1994
{
public:
    Yan2012(FlashParams& flash_params, int nc_, int np_) : Michelsen1994(flash_params, nc_, np_) { }

    virtual int solve_rr(std::vector<double>& z_, std::vector<double>& K_, const std::vector<int>& nonzero_comp_={}) override;
    virtual std::vector<double> getx() override;

private:
    virtual double calc_obj(std::vector<double>& nu_) override;
    void update_g(std::vector<double>& Ei_inv);

};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_RR_RR_H
//--------------------------------------------------------------------------
