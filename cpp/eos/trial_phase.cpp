#include <iostream>
#include "dartsflash/global/global.hpp"
#include "dartsflash/eos/trial_phase.hpp"

TrialPhase::TrialPhase(std::string eos_name_, std::vector<double>& Y_)
{
	this->Y = Y_;
	this->eos_name = eos_name_;
	this->tpd = 0.;
	this->preferred = true;
}

// TrialPhase::TrialPhase(TrialPhase& trial_comp)
// {
//     this->eos
// }

void TrialPhase::print_point(std::string text)
{
	std::cout << text << ":\n";
	print("Y", this->Y);
	print("tpd", this->tpd);
	print("eos", this->eos_name);
	print("preferred", this->preferred);
}
