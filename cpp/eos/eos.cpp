#include <iostream>
#include <cmath>
#include <vector>
#include <numeric>
#include <algorithm>
#include <cassert>

#include "dartsflash/global/global.hpp"
#include "dartsflash/global/units.hpp"
#include "dartsflash/eos/eos.hpp"

EoS::EoS(CompData& comp_data) 
{
    this->nc = comp_data.nc;
    this->ni = comp_data.ni;
    this->ns = nc + ni;
    this->units = comp_data.units;
	
    this->dlnphidP.resize(ns);
    this->dlnphidT.resize(ns);
    this->dlnphidn.resize(ns*ns);

    this->compdata = comp_data;
}

void EoS::set_eos_range(int i, const std::vector<double>& range)
{
    // Define range of applicability for specific EoS
    this->eos_range[i] = range;
    return;
}

bool EoS::eos_in_range(std::vector<double>::iterator n_it)
{
    // Check if EoS is applicable for specific state according to specified ranges
    for (auto& it: this->eos_range) {
        double N_ = std::accumulate(n_it, n_it + this->ns, 0.);
        double xi = *(n_it + it.first) / N_;
		if (xi < it.second[0] || xi > it.second[1])
        {
            return false;
        }
	}
    return true;
}

void EoS::parameters(double p_, double T_, std::vector<double>& n, int start_idx, bool second_order)
{
    // Calculate composition-independent and composition-dependent EoS-parameters
    this->parameters(p_, T_);
    this->parameters(n.begin() + start_idx, second_order);
    return;
}

std::vector<double> EoS::lnphi()
{
    // Return vector of lnphi for each component
    std::vector<double> ln_phi(ns);
    for (int i = 0; i < ns; i++)
    {
        ln_phi[i] = this->lnphii(i);
    }
    return ln_phi;
}

std::vector<double> EoS::dlnphi_dn() {
    for (int i = 0; i < ns; i++)
    {
        for (int j = 0; j < ns; j++)
        {
            dlnphidn[i * ns + j] = this->dlnphii_dnj(i, j);
        }
    }
    return dlnphidn;
}

std::vector<double> EoS::dlnphi_dP() {
    for (int i = 0; i < ns; i++)
    {
        dlnphidP[i] = this->dlnphii_dP(i);
    }
    return dlnphidP;
}

std::vector<double> EoS::dlnphi_dT() {
    for (int i = 0; i < ns; i++)
    {
        dlnphidT[i] = this->dlnphii_dT(i);
    }
    return dlnphidT;
}

std::vector<double> EoS::fugacity(double p_, double T_, std::vector<double>& x_)
{
	// Calculate component fugacities in mixture
	std::vector<double> fi(nc);

	// Calculate fugacity coefficients and fugacity
    this->parameters(p_, T_, x_, 0, false);
    for (int i = 0; i < nc; i++)
    {
        fi[i] = std::exp(this->lnphii(i)) * x_[i] * p;
    }
	return fi;
}

double EoS::G_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal + residual Gibbs free energy
    double Gi = this->compdata.ideal.G(x_.begin() + start_idx);
    double Gr = this->Gr_TP(p_, T_, x_, start_idx) / (this->units.R * T_);
    return Gi + Gr;
}

double EoS::Gr_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate Gibbs energy of mixture at P, T, x
    std::vector<double> GriTP = this->Gri_TP(p_, T_, x_, start_idx);
    
    double G = 0.;
    bool nans = false;
    for (int i = 0; i < ns; i++)
    {
        if (!std::isnan(GriTP[i]))
        {
            G += x_[start_idx + i] * GriTP[i];
        }
        else
        {
            nans = true;
        }
    }
    // If all NANs, G will be equal to zero, so return NAN; else return G
    return (nans && G == 0.) ? NAN : G;
}

std::vector<double> EoS::Gri_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar Gibbs energy at P,T,x
    if (this->eos_in_range(x_.begin() + start_idx))
    {
        std::vector<double> GriTP(ns);

        this->parameters(p_, T_, x_, start_idx, false);
        for (int i = 0; i < ns; i++)
        {
            if (x_[start_idx + i] > 0.)
            {
                GriTP[i] = this->lnphii(i) * this->units.R * T;
            }
            else
            {
                GriTP[i] = NAN;
            }
        }
        return GriTP;
    }
    else
    {
        return std::vector<double>(ns, NAN);
    }
}

double EoS::H_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal + residual enthalpy
    double Hi = this->compdata.ideal.H(T_, x_.begin() + start_idx);
    double Hr = this->Hr_TP(p_, T_, x_, start_idx) / this->units.R;
    return Hi + Hr;  // H/R
}

double EoS::Hr_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate residual enthalpy of mixture at P, T, x
    std::vector<double> HriTP = this->Hri_TP(p_, T_, x_, start_idx);

    double H = 0.;
    bool nans = false;
    for (int i = 0; i < ns; i++)
    {
        if (!std::isnan(HriTP[i]))
        {
            H += x_[start_idx + i] * HriTP[i];
        }
        else
        {
            nans = true;
        }
    }
    // If all NANs, H will be equal to zero, so return NAN; else return H
    return (nans && H == 0.) ? NAN : H;
}

std::vector<double> EoS::Hri_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar enthalpy at P,T,x
    std::vector<double> HriTP(ns);

    this->parameters(p_, T_, x_, start_idx, true);
    dlnphidT = this->dlnphi_dT();
    double RT2 = this->units.R * std::pow(T, 2);
    for (int i = 0; i < ns; i++)
    {
        HriTP[i] = -RT2 * dlnphidT[i];
    }
    return HriTP;
}

/*
double EoS::S_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal + residual entropy
    double Si = this->compdata.ideal.S(T_, x_.begin() + start_idx);
    double Sr = this->Sr_TP(p_, T_, x_, start_idx);
    return Si + Sr;
}
*/

double EoS::Sr_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate residual entropy of mixture at P, T, x
    std::vector<double> SriTP = this->Sri_TP(p_, T_, x_, start_idx);

    double S = 0.;
    bool nans = false;
    for (int i = 0; i < ns; i++)
    {
        if (!std::isnan(SriTP[i]))
        {
            S += x_[start_idx + i] * SriTP[i];
        }
        else
        {
            nans = true;
        }
    }
    // If all NANs, S will be equal to zero, so return NAN; else return S
    return (nans && S == 0.) ? NAN : S;
}

std::vector<double> EoS::Sri_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar entropy of mixture at P, T, x
    std::vector<double> SriTP(ns);
    std::vector<double> HriTP = this->Hri_TP(p_, T_, x_, start_idx);
    std::vector<double> GriTP = this->Gri_TP(p_, T_, x_, start_idx);

    double T_inv = 1./T;
    for (int i = 0; i < ns; i++)
    {
        SriTP[i] = (HriTP[i] - GriTP[i]) * T_inv;
    }
    return SriTP;
}

/*
double EoS::A_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal + residual Helmholtz free energy
    double Ai = this->compdata.ideal.A(T_, x_.begin() + start_idx);
    double Ar = this->Ar_TP(p_, T_, x_, start_idx);
    return Ai + Ar;
}

double EoS::Ar_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate residual Helmholtz free energy of mixture at P, T, x
}

std::vector<double> EoS::Ari_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar Helmholtz free energy of mixture at P, T, x
}

double EoS::U_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Ideal + residual internal energy
    double Ui = this->compdata.ideal.U(T_, x_.begin() + start_idx);
    double Ur = this->Ur_TP(p_, T_, x_, start_idx);
    return Ui + Ur;
}

double EoS::Ur_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate residual internal energy of mixture at P, T, x
}

std::vector<double> EoS::Uri_TP(double p_, double T_, std::vector<double>& x_, int start_idx)
{
    // Calculate partial molar internal energy of mixture at P, T, x
}
*/

double EoS::property_of_mixing(std::vector<double>& x_, std::vector<double>& mixture_prop, std::vector<double>& pure_prop)
{
    // Calculate Gibbs energy of mixing of mixture at P, T, x
    if (this->eos_in_range(x_.begin()))
    {
        double prop_of_mixing = 0.;
        for (int i = 0; i < ns; i++)
        {
            if (x_[i] > 0.)
            {
                prop_of_mixing += x_[i] * (mixture_prop[i] - pure_prop[i]);
            }
        }
        return prop_of_mixing;
    }
    else
    {
        return NAN;
    }
}

std::vector<double> EoS::dlnphi_dn_num(double p_, double T_, std::vector<double>& n_, double dn) {
    // Numerical derivative of fugacity coefficient w.r.t. composition
    this->parameters(p_, T_, n_, 0, false);
    std::vector<double> ln_phi = this->lnphi();
    for (int j = 0; j < ns; j++)
    {
        n_[j] += dn;
        this->parameters(p_, T_, n_, 0, false);
        std::vector<double> ln_phi1 = this->lnphi();
        n_[j] -= dn;
        for (int i = 0; i < ns; i++)
        {
            this->dlnphidn[j*ns + i] = (ln_phi1[i]-ln_phi[i])/dn;
        }
    }
    return dlnphidn;
}

std::vector<double> EoS::dlnphi_dT_num(double p_, double T_, std::vector<double>& n_, double dT) {
    // Numerical derivative of fugacity coefficient w.r.t. temperature
    std::vector<double> ln_phi = lnphi();

    T_ += dT;
    this->parameters(p_, T_, n_, 0, false);
    std::vector<double> ln_phi1 = this->lnphi();
    T_ -= dT;
    for (int i = 0; i < ns; i++)
    {
        this->dlnphidT[i] = (ln_phi1[i]-ln_phi[i])/dT;
    }
    return this->dlnphidT;
}

std::vector<double> EoS::dlnphi_dP_num(double p_, double T_, std::vector<double>& n_, double dP) {
    // Numerical derivative of fugacity coefficient w.r.t. pressure
    std::vector<double> ln_phi = lnphi();

    p_ += dP;
    this->parameters(p_, T_, n_, 0, false);
    std::vector<double> ln_phi1 = this->lnphi();
    p_ -= dP;
    for (int i = 0; i < ns; i++)
    {
        this->dlnphidP[i] = (ln_phi1[i]-ln_phi[i])/dP;
    }
    return this->dlnphidP;
}

double EoS::dxj_to_dnk(std::vector<double>& dlnphiidxj, std::vector<double>::iterator n_it, int k) {
    // Translate from dlnphii/dxj to dlnphii/dnk
	// dlnphii/dnk = 1/V * [dlnphii/dxk - sum_j xj dlnphii/dxj]
    double nT_inv = 1./std::accumulate(n_it, n_it + this->ns, 0.);
	double dlnphiidnk = dlnphiidxj[k];
	for (int j = 0; j < ns; j++)
	{
        double nj = *(n_it + j);
		dlnphiidnk -= nj * nT_inv * dlnphiidxj[j];
	}
	dlnphiidnk *= nT_inv;
    return dlnphiidnk;
}

std::vector<double> EoS::dxj_to_dnk(std::vector<double>& dlnphiidxj, std::vector<double>::iterator n_it)
{
    // Translate from dlnphii/dxj to dlnphii/dnk
	// dlnphii/dnk = 1/V * [dlnphii/dxk - sum_j xj dlnphii/dxj]
    double nT_inv = 1./std::accumulate(n_it, n_it + this->ns, 0.);
    std::vector<double> dlnphiidnk(ns*ns);

    for (int i = 0; i < ns; i++)
    {
        double sum_xj = 0.;
        for (int j = 0; j < ns; j++)
        {
            double nj = *(n_it + j);
            sum_xj += nj * nT_inv * dlnphiidxj[i*ns + j];
        }
        for (int k = 0; k < ns; k++)
        {
            dlnphiidnk[i*ns + k] = nT_inv * (dlnphiidxj[i*ns + k] - sum_xj);
        }
    }
    return dlnphiidnk;
}
