//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_AQ_AQ_H
#define OPENDARTS_FLASH_EOS_AQ_AQ_H
//--------------------------------------------------------------------------

#include "dartsflash/eos/eos.hpp"
#include "dartsflash/global/global.hpp"
#include "dartsflash/global/components.hpp"

#include <map>

class AQBase;

class AQEoS : public EoS
{
public:
	enum class Model : int { Ziabakhsh2012 = 0, Jager2003 };
	enum CompType : int { water = 0, solute, ion };

protected:
	std::vector<double>::iterator n_iterator;

	std::map<CompType, Model> evaluator_map;
	std::map<Model, AQBase*> evaluators;

public:
	AQEoS(CompData& comp_data);
	AQEoS(CompData& comp_data, AQEoS::Model model);
	AQEoS(CompData& comp_data, std::map<AQEoS::CompType, AQEoS::Model>& evaluator_map_);
	AQEoS(CompData& comp_data, std::map<AQEoS::CompType, AQEoS::Model>& evaluator_map_, std::map<AQEoS::Model, AQBase*>& evaluators_);

	EoS* getCopy() override { return new AQEoS(*this); }

	void parameters(double p_, double T_) override;
	void parameters(std::vector<double>::iterator n_it, bool second_order=true) override;

	double lnphii(int i) override;
	double dlnphii_dP(int i) override;
	double dlnphii_dT(int i) override;
	double dlnphii_dnj(int i, int j) override;
};

class AQBase
{
protected:
	CompData compdata;
	int nc, ni, ns, water_index;
	double p, T;
	
	std::vector<std::string> species;
	std::vector<double> x, m_s;
	std::vector<int> charge;

public:
	AQBase(CompData& comp_data);

	virtual AQBase* getCopy() = 0;

	virtual void parameters(double p_, double T_, AQEoS::CompType comp_type) = 0;
	virtual void parameters(std::vector<double>& x_, bool second_order, AQEoS::CompType comp_type) = 0;

	virtual double lnphii(int i) = 0;
	virtual double dlnphii_dP(int i) = 0;
	virtual double dlnphii_dT(int i) = 0;
	virtual double dlnphii_dxj(int i, int j) = 0;

protected:
	void species_molality();
	double dmi_dxi();
	double dmi_dxw(int i);
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_AQ_AQ_H
//--------------------------------------------------------------------------
