//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_TRIALPHASE_H
#define OPENDARTS_FLASH_EOS_TRIALPHASE_H
//--------------------------------------------------------------------------

#include <vector>
#include <string>

struct TrialPhase
{
	double tpd;
	std::string eos_name;
	std::vector<double> Y;
	bool preferred = true;

	TrialPhase() {}
	// TrialPhase(const TrialPhase&) = default;
	TrialPhase(std::string eos_name_, std::vector<double>& Y_);

	void print_point(std::string text="TrialPhase");
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_TRIALPHASE_H
//--------------------------------------------------------------------------
