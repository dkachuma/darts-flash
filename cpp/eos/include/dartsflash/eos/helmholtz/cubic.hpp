//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_HELMHOLTZ_CUBIC_H
#define OPENDARTS_FLASH_EOS_HELMHOLTZ_CUBIC_H
//--------------------------------------------------------------------------

#include <complex>
#include "dartsflash/eos/helmholtz/helmholtz.hpp"
#include "dartsflash/eos/helmholtz/mix.hpp"
#include "dartsflash/global/components.hpp"

class CubicParams;

class CubicEoS : public HelmholtzEoS
{
protected:
	double d1, d2, Vr;
	Mix* mix;

	// 0th order
    double g, f, A, B;
    // 1st order
    double g_V, g_B, f_V, f_B;
    std::vector<double> B_i, D_i;
    // 2nd order
    double g_VV, g_BV, g_BB, f_VV, f_BV, f_BB;
    std::vector<double> B_ij, D_ij, D_iT;
    double D_T, D_TT;

public:
	enum CubicType { PR = 0, SRK };

	CubicEoS(CompData& comp_data, CubicEoS::CubicType type);
	CubicEoS(CompData& comp_data, CubicParams& cubic_params);

	EoS* getCopy() override { return new CubicEoS(*this); }

	// Pure component and mixture parameters
	void parameters(double p_, double T_) override;

	// Pressure function and derivatives
	std::vector<std::complex<double>> Z() override;

protected:
	// Volume
	double V() override;
	
	// Reduced Helmholtz function and derivatives
	double F() override;
	double dF_dV() override;
	double dF_dT() override;
	double dF_dni(int i) override;
    double d2F_dnidnj(int i, int j) override;
    double d2F_dTdni(int i) override;
    double d2F_dVdni(int i) override;
    double d2F_dTdV() override;
    double d2F_dV2() override;
    double d2F_dT2() override;

	// Elements of Helmholtz function
	void zeroth_order(std::vector<double>::iterator n_it) override;
	void zeroth_order(std::vector<double>::iterator n_it, double V_) override;
	void zeroth_order(double V_) override;
	void first_order(std::vector<double>::iterator n_it) override;
	void second_order(std::vector<double>::iterator n_it) override;

	double F_n(), F_T(), F_V(), F_B(), F_D();
	double F_nV(), F_nB(), F_TT(), F_BT(), F_DT(), F_BV(), F_BB(), F_DV(), F_BD(), F_TV(), F_VV();

public:
	int mix_dT_test(double T_, std::vector<double>& n, double tol);
};

class CubicParams
{
public:
	double d1, d2;
	Mix *mix;

	CubicParams() {}
	CubicParams(double d1_, double d2_, double omegaA, double omegaB, std::vector<double>& kappa, CompData& comp_data);
	~CubicParams() = default;
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_HELMHOLTZ_CUBIC_H
//--------------------------------------------------------------------------
