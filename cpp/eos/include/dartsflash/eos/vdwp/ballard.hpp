//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_VDWP_BALLARD_H
#define OPENDARTS_FLASH_EOS_VDWP_BALLARD_H
//--------------------------------------------------------------------------

#include "dartsflash/eos/vdwp/vdwp.hpp"

namespace ballard {
	class Integral
	{
	protected:
    	double pp, TT;
		std::string phase;
    
	public:
		Integral(std::string phase_) { phase = phase_; }
	};

	class HB : public Integral
	{
	public:
		HB(std::string phase_) : Integral(phase_) {}

		double f(double T);
		double F(double T);

		double dFdT(double T);
	};

	class VB : public Integral
	{
	public:
		VB(std::string phase_) : Integral(phase_) {}

		double f(double p, double T);
		double F(double p, double T);

		double dFdP(double p, double T);
		double dFdT(double p, double T);
	};

	class VH : public Integral
	{
	private:
		std::vector<std::string> components;
		int nc, water_index;

	public:
		VH(std::string phase_, std::vector<std::string> components_);

		double f(double p, double T, std::vector<double> theta);
		/*
		double F(double p, double T, std::vector<double> theta);

		double dFdP(double p, double T, std::vector<double> theta);
		double dFdT(double p, double T, std::vector<double> theta, std::vector<double> dthetadT);
		std::vector<double> dFdxj(double p, double T, std::vector<double> theta, std::vector<double> dthetadxj);

		double dfdT(double p, double T, std::vector<double> theta, std::vector<double> dthetadT);
		std::vector<double> dfdxj(double p, double T, std::vector<double> theta, std::vector<double> dthetadxj);
		*/
	};
	
	class Kihara : public Integral
	{
	public:
		double R0, R1;
		int cage_index, R1_index;

	public:
		Kihara(double r0, double r1, std::string phase_) : Integral(phase_) { R0 = r0; R1 = r1; }

		double w(double r, std::string component);

		double f(double r, double T, std::string component);
		double F(double T, std::string component);

		double dfdT(double r, double T, std::string component);
		double dFdT(double T, std::string component);
	};
}

class Ballard : public VdWP
{
private:
	double g_w0, g_B0, h_B0, v_B0;
	double g_B, h_B;

	// member variables needed for integrals
	int cage_index, shell_index, R1_index; // cage, shell indices and index of innermost shell in cage, needed for integrals
	std::vector<int> zn, n_shells; // #waters in cage, #waters in shell, #shells in cage
	std::vector<double> Rn; // radius of shells, #guests per unit cell, #cages per H2O per unit cell

public:
	Ballard(CompData& comp_data, std::string hydrate_type);
	EoS* getCopy() override { return new Ballard(*this); }

	void parameters(double p_, double T_) override;
	void parameters(std::vector<double>::iterator n_it, bool second_order=true) override;

	double V(double p_, double T_, std::vector<double>& n) override;
	double fw(std::vector<double>& fi) override;

private:
	// Fugacity of water
	double dfw_dP(std::vector<double>& dfidP) override;
	double dfw_dT(std::vector<double>& dfidT) override;
	std::vector<double> dfw_dxj(std::vector<double>& dfidxj) override;

	// Langmuir constant
	std::vector<double> calc_Ckm() override;
	std::vector<double> dCkm_dP() override;
	std::vector<double> dCkm_dT() override;

};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_VDWP_BALLARD_H
//--------------------------------------------------------------------------
