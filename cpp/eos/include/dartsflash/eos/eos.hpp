//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_EOS_EOS_H
#define OPENDARTS_FLASH_EOS_EOS_H
//--------------------------------------------------------------------------

#include <vector>
#include <string>
#include <map>
#include "dartsflash/global/components.hpp"
#include "dartsflash/global/units.hpp"

class EoS
{
protected:
	int nc, ni = 0, ns;
	double p{ -1. }, T{ -1. };
	std::map<int, std::vector<double>> eos_range;
	std::vector<double> dlnphidn, dlnphidT, dlnphidP;
	CompData compdata;
	Units units;

public:
	EoS(CompData& comp_data);
	virtual ~EoS() = default;

	EoS(const EoS&) = default;
	virtual EoS* getCopy() = 0;

	void set_eos_range(int i, const std::vector<double>& range);
	bool eos_in_range(std::vector<double>::iterator n_it);

	virtual void parameters(double p_, double T_) = 0;
	virtual void parameters(std::vector<double>::iterator n_it, bool second_order=true) = 0;
	virtual void parameters(double p_, double T_, std::vector<double>& n_, int start_idx=0, bool second_order=true);

	virtual double lnphii(int i) = 0;
	virtual double dlnphii_dP(int i) { return dlnphidP[i]; }
	virtual double dlnphii_dT(int i) { return dlnphidT[i]; }
	virtual double dlnphii_dnj(int i, int j) { return dlnphidn[i*nc + j]; };

	std::vector<double> lnphi();
	virtual std::vector<double> dlnphi_dP();
	virtual std::vector<double> dlnphi_dT();
	virtual std::vector<double> dlnphi_dn();
	std::vector<double> fugacity(double p_, double T_, std::vector<double>& x_);

	// "Total" properties (reference conditions arbitrary)
	double G_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	double H_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// double S_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// double A_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// double U_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);

	// Residual properties
	virtual double Gr_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	virtual double Hr_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	virtual double Sr_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// virtual double Ar_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// virtual double Ur_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	std::vector<double> Gri_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	std::vector<double> Hri_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	std::vector<double> Sri_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// std::vector<double> Ari_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);
	// std::vector<double> Uri_TP(double p_, double T_, std::vector<double>& x_, int start_idx=0);

	// Mixing properties
	double property_of_mixing(std::vector<double>& x_, std::vector<double>& mixture_prop, std::vector<double>& pure_prop);

protected:
	std::vector<double> dlnphi_dP_num(double p_, double T_, std::vector<double>& n_, double dp);
	std::vector<double> dlnphi_dT_num(double p_, double T_, std::vector<double>& n_, double dT);
	std::vector<double> dlnphi_dn_num(double p_, double T_, std::vector<double>& n_, double dn);

	double dxj_to_dnk(std::vector<double>& dlnphiidxj, std::vector<double>::iterator n_it, int k);
	std::vector<double> dxj_to_dnk(std::vector<double>& dlnphiidxj, std::vector<double>::iterator n_it);
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_EOS_EOS_H
//--------------------------------------------------------------------------
