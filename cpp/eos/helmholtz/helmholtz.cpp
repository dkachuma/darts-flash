#include <iostream>
#include <cmath>
#include <vector>
#include <numeric>

#include "dartsflash/eos/helmholtz/helmholtz.hpp"

HelmholtzEoS::HelmholtzEoS(CompData& comp_data) : EoS(comp_data) {}

// Evaluation of EoS at (T, V, n)
void HelmholtzEoS::parameters(std::vector<double>::iterator n_it, bool second_order)
{
    // Calculate zero'th order parameters for (P, T, n) specification. This includes calculation of V(P, T, n)
    N = std::accumulate(n_it, n_it + this->nc, 0.);
    for (auto it: this->preferred_roots)
    {
        // it.first: i
        // it.second: {x, root_flag}
        if (*(n_it + it.first) / N >= it.second.first)
        {
            this->set_root_flag(it.second.second);
        }
    }
    this->zeroth_order(n_it);

    // Calculate first/second order parameters and elements of reduced Helmholtz function F
    if (!second_order)
    {
        this->first_order(n_it);
    }
    else
    {
        this->second_order(n_it);
    }
    return;
}
void HelmholtzEoS::parameters_TV(double T_, double V_, std::vector<double>& n, int start_idx, bool second_order)
{
    // Calculate zero'th order parameters for (V, T, n) specification
    auto n_it = n.begin() + start_idx;
    N = std::accumulate(n_it, n_it + this->nc, 0.);
    this->parameters(1., T_);

    // Calculate first/second order parameters and elements of reduced Helmholtz function F
    this->zeroth_order(n_it, V_);
    if (!second_order)
    {
        this->first_order(n_it);
    }
    else
    {
        this->second_order(n_it);
    }

    // Calculate pressure
    this->p = P();
    return;
}

// Overloaded pressure P(T, V, n) and volume V(P, T, n) functions
double HelmholtzEoS::P(double T_, double V_, std::vector<double>& n) 
{
    this->parameters_TV(T_, V_, n, 0, false);
	return this->P();
}
double HelmholtzEoS::V(double p_, double T_, std::vector<double>& n) 
{
    this->parameters(p_, T_, n, 0, false);
	return this->V();
}

// Pressure function and derivatives
double HelmholtzEoS::P() 
{
    return -this->units.R * T * this->dF_dV() + N * this->units.R * T / v;
}
double HelmholtzEoS::Z(double V_) 
{
    return p * V_ / (N * this->units.R * T);
}
double HelmholtzEoS::dP_dV() 
{
    return - this->units.R * T * this->d2F_dV2() - N * this->units.R * T / std::pow(v, 2);
}
double HelmholtzEoS::dP_dT() 
{
    return - this->units.R * T * this->d2F_dTdV() + p / T;
}
double HelmholtzEoS::dP_dni(int i) 
{
    return - this->units.R * T * this->d2F_dVdni(i) + this->units.R * T / v;
}
double HelmholtzEoS::dV_dni(int i) 
{
    return - this->dP_dni(i) / this->dP_dV();
}
double HelmholtzEoS::dV_dT() 
{
    return - this->dP_dT() / this->dP_dV();
}
double HelmholtzEoS::dT_dni(int i) 
{
    return - this->dP_dni(i) / this->dP_dT();
}

// Fugacity coefficient and derivatives
double HelmholtzEoS::lnphii(int i) 
{
    return this->dF_dni(i) - std::log(z);
}
double HelmholtzEoS::dlnphii_dnj(int i, int j) 
{
    return this->d2F_dnidnj(i, j) + 1./N + 1./(this->units.R * T) * this->dP_dni(j) * this->dP_dni(i) / this->dP_dV();
}
double HelmholtzEoS::dlnphii_dT(int i) 
{
    return this->d2F_dTdni(i) + 1./T - this->dV_dni(i) / (this->units.R * T) * this->dP_dT();
}
double HelmholtzEoS::dlnphii_dP(int i) 
{
    return this->dV_dni(i) / (this->units.R * T) - 1./p;
}

// Residual bulk properties
double HelmholtzEoS::Ar_TV(double T_, double V_, std::vector<double>& n, int start_idx) 
{
    // Ar_TV = F * RT
    this->parameters_TV(T_, V_, n, start_idx, false);
    return this->F() * this->units.R * T;
}
double HelmholtzEoS::Sr_TV(double T_, double V_, std::vector<double>& n, int start_idx) 
{
    // Sr_TV = -R (T dF/dT + F)
    this->parameters_TV(T_, V_, n, start_idx, true);
    return -this->units.R * (T * this->dF_dT() + this->F());
}
double HelmholtzEoS::Ur_TV(double T_, double V_, std::vector<double>& n, int start_idx) 
{
    // Ur_TV = Ar_TV + T * Sr_TV
    this->parameters_TV(T_, V_, n, start_idx, true);

    // Ar_TV = F * RT
    // Sr_TV = -R (T dF/dT + F)
    double ArTV = this->F() * this->units.R * T;
    double SrTV = -this->units.R * (T * this->dF_dT() + this->F());
    
    return ArTV + T * SrTV;
}
double HelmholtzEoS::Hr_TV(double T_, double V_, std::vector<double>& n, int start_idx) 
{
    // Hr_TV = Ur_TV + PV - nRT
    double UrTV = this->Ur_TV(T_, V_, n, start_idx);

    return UrTV + p * v - N * this->units.R * T;
}
double HelmholtzEoS::Gr_TV(double T_, double V_, std::vector<double>& n, int start_idx) 
{
    // Gr_TV = Ar_TV + PV - nRT
    // Ar_TV = F * RT
    double ArTV = this->Ar_TV(T_, V_, n, start_idx);

    return ArTV + p * v - N * this->units.R * T;
}
double HelmholtzEoS::Ar_TP(double p_, double T_, std::vector<double>& n, int start_idx) 
{
    // Ar_TP = Ar_TV - nRT ln Z
    this->parameters(p_, T_, n, start_idx, false);

    // Ar_TV = F * RT
    double ArTV = this->F() * this->units.R * T;

    return ArTV - N * this->units.R * T * std::log(z);
}
double HelmholtzEoS::Sr_TP(double p_, double T_, std::vector<double>& n, int start_idx) 
{
    // Sr_TP = Sr_TV + nR ln Z
    this->parameters(p_, T_, n, start_idx, true);

    // Sr_TV = -R (T dF/dT + F)
    double SrTV = -this->units.R * (T * this->dF_dT() + this->F());

    return SrTV + N * this->units.R * std::log(z);
}
double HelmholtzEoS::Ur_TP(double p_, double T_, std::vector<double>& n, int start_idx) 
{
    // Ur_TP = Ur_TV
    this->parameters(p_, T_, n, start_idx, true);

    // Ur_TV = Ar_TV + T * Sr_TV
    // Ar_TV = F * RT
    // Sr_TV = -R (T dF/dT + F)
    double ArTV = this->F() * this->units.R * T;
    double SrTV = -this->units.R * (T * this->dF_dT() + this->F());
    
    return ArTV + T * SrTV;
}
double HelmholtzEoS::Hr_TP(double p_, double T_, std::vector<double>& n, int start_idx)
{
    // Hr_TP = Hr_TV = Ur_TV + PV - nRT
    // Ur_TV = Ur_TP
    double UrTV = this->Ur_TP(p_, T_, n, start_idx);

    return UrTV + p * v - N * this->units.R * T;
}
double HelmholtzEoS::Gr_TP(double p_, double T_, std::vector<double>& n, int start_idx)
{
    // Gr_TP = Gr_TV - nRT ln Z
    this->parameters(p_, T_, n, start_idx, false);

    // Gr_TV = Ar_TV + PV - nRT
    // Ar_TV = F * RT
    double ArTV = this->F() * this->units.R * T;
    double GrTV = ArTV + p * v - N * this->units.R * T;

    return GrTV - N * this->units.R * T * std::log(z);
}

double HelmholtzEoS::Cvr(double p_, double T_, std::vector<double>& n, int start_idx) 
{
    // Cvr/R = - T^2 d2F/dT2 - 2T dF/dT
    this->parameters(p_, T_, n, start_idx, true);
    return -std::pow(T, 2) * this->d2F_dT2() - 2. * T * this->dF_dT();
}
double HelmholtzEoS::Cv(double p_, double T_, std::vector<double>& n, int start_idx)
{
    // Total heat capacity at constant volume Cv/R
    double cvr = this->Cvr(p_, T_, n, start_idx);
    double cvi = 0.;
    for (int i = 0; i < ns; i++)
    {
        cvi += n[i] * this->compdata.ideal.cvi(T, i);
    }
    return cvi + cvr;
}
double HelmholtzEoS::Cpr(double p_, double T_, std::vector<double>& n, int start_idx) 
{
    // Cpr/R = Cvr/R - T/R (dP/dT)^2/ (dP/dV) - n
    double cvr = this->Cvr(p_, T_, n, start_idx);
    double cpr = cvr - T/this->units.R * std::pow(this->dP_dT(), 2) / this->dP_dV() - N;
    return cpr;
}
double HelmholtzEoS::Cp(double p_, double T_, std::vector<double>& n, int start_idx)
{
    // Total heat capacity at constant pressure Cp/R
    double cpr = this->Cpr(p_, T_, n, start_idx);
    double cpi = 0.;
    for (int i = 0; i < ns; i++)
    {
        cpi += n[i] * this->compdata.ideal.cpi(T, i);
    }
    return cpi + cpr;
}

double HelmholtzEoS::vs(double p_, double T_, std::vector<double>& n, int start_idx) 
{
    // Sound speed vs^2 = v / (β Mw)
    // β = -1/v Cvr/Cpr / (dP/dV)
    auto begin = n.begin() + start_idx;
    double Mw = std::inner_product(begin, begin + ns, this->compdata.Mw.begin(), 0.);
    double cv = this->Cv(p_, T_, n, start_idx);
    double cp = this->Cp(p_, T_, n, start_idx);
    double beta = -1./v * cv/cp / this->dP_dV();
    double VS2 = v / (beta * Mw * 1e-3) * M_R / this->units.R;
    return std::sqrt(VS2);
}
double HelmholtzEoS::JT(double p_, double T_, std::vector<double>& n, int start_idx) 
{
    // Joule-Thomson coefficient μ_JT = -1/Cp (v + T (dP/dT)/(dP/dV))
    double cp = this->Cp(p_, T_, n, start_idx) * this->units.R;
    return -1. / cp * (v - T * this->dV_dT());
}

// EoS CONSISTENCY TESTS
int HelmholtzEoS::derivatives_test(double p_, double T_, std::vector<double>& n, double tol)
{
    // Test derivatives of Helmholtz function F(T,V,n) = Ar(T,V,n)/RT
    int error_output = 0;
    double T0 = T_;
    double V0 = this->V(p_, T_, n);
    std::vector<double> n0 = n;
    double dX = 1e-5;

    // Analytical derivatives
    this->parameters_TV(T0, V0, n0, 0, true);
    double F0 = this->F();
    double dV = this->dF_dV();
    double dT = this->dF_dT();
    double dTdV = this->d2F_dTdV();
    double dV2 = this->d2F_dV2();
    double dT2 = this->d2F_dT2();

    std::vector<double> dni(nc), dTdni(nc), dVdni(nc), dnidnj(nc*nc);
    for (int i = 0; i < nc; i++)
    {
        dni[i] = this->dF_dni(i);
        dTdni[i] = this->d2F_dTdni(i);
        dVdni[i] = this->d2F_dVdni(i);
        for (int j = 0; j < nc; j++)
        {
            dnidnj[i*nc + j] = this->d2F_dnidnj(i, j);
        }
    }

    // Numerical derivatives
    double d;

    // dF/dV
    this->parameters_TV(T0, V0 + V0*dX, n, 0, true);
    double dV_num = (this->F() - F0) / (V0*dX);
    d = std::log(std::fabs(dV + 1e-15)) - std::log(std::fabs(dV_num + 1e-15));
    if (std::fabs(d) > tol) { print("dF/dV != dF/dV", std::vector<double>{dV, dV_num, d}); error_output++; }

    // dF/dT
    this->parameters_TV(T0 + T0*dX, V0, n, 0, true);
    double dT_num = (this->F() - F0) / (T0*dX);
    d = std::log(std::fabs(dT + 1e-15)) - std::log(std::fabs(dT_num + 1e-15));
    if (std::fabs(d) > tol) { print("dF/dT != dF/dT", std::vector<double>{dT, dT_num, d}); error_output++; }

    // dF/dni
    for (int i = 0; i < nc; i++)
    {
        n = n0;
        n[i] += dX*n0[i];
        this->parameters_TV(T0, V0, n, 0, true);
        double dni_num = (this->F() - F0) / (n0[i]*dX);
        d = std::log(std::fabs(dni[i] + 1e-15)) - std::log(std::fabs(dni_num + 1e-15));
        if (std::fabs(d) > tol) { print("i", i); print("dF/dni != dF/dni", std::vector<double>{dni[i], dni_num, d}); error_output++; }

        // d2F/dnidnj
        double dni1, dni_1;
        for (int j = 0; j < nc; j++)
        {
            n[j] += dX*n0[j];
            this->parameters_TV(T0, V0, n, 0, true);
            dni1 = this->dF_dni(i);
            n[j] -= dX*n0[j];

            n[j] -= dX*n0[j];
            this->parameters_TV(T0, V0, n, 0, true);
            dni_1 = this->dF_dni(i);
            n[j] += dX*n0[j];

            double dnidnj_num = (dni1 - dni_1) / (2*n0[j]*dX);
            d = std::log(std::fabs(dnidnj[i*nc + j] + 1e-15)) - std::log(std::fabs(dnidnj_num + 1e-15));
            if (std::fabs(d) > tol) 
            { 
                print("i, j", std::vector<int>{i, j}); print("d2F/dnidnj != d2F/dnidnj", std::vector<double>{dnidnj[i*nc + j], dnidnj_num, d}); 
                error_output++; 
            }
        }

        // d2F/dTdni
        this->parameters_TV(T0 + T0*dX, V0, n, 0, true);
        dni1 = this->dF_dni(i);
        this->parameters_TV(T0 - T0*dX, V0, n, 0, true);
        dni_1 = this->dF_dni(i);
        double dTdni_num = (dni1 - dni_1) / (2*T0*dX);
        d = std::log(std::fabs(dTdni[i] + 1e-15)) - std::log(std::fabs(dTdni_num + 1e-15));
        if (std::fabs(d) > tol) { print("i", i); print("d2F/dTdni != d2F/dTdni", std::vector<double>{dTdni[i], dTdni_num, d}); error_output++; }

        // d2F/dVdni
        this->parameters_TV(T0, V0 + V0*dX, n, 0, true);
        dni1 = this->dF_dni(i);
        this->parameters_TV(T0, V0 - V0*dX, n, 0, true);
        dni_1 = this->dF_dni(i);
        double dVdni_num = (dni1 - dni_1) / (2*V0*dX);
        d = std::log(std::fabs(dVdni[i] + 1e-15)) - std::log(std::fabs(dVdni_num + 1e-15));
        if (std::fabs(d) > tol) { print("i", i); print("d2F/dVdni != d2F/dVdni", std::vector<double>{dVdni[i], dVdni_num, d}); error_output++; }

        n[i] -= dX*n0[i];
    }

    // d2F/dVdT and d2F/dTdV
    this->parameters_TV(T0 - dX*T0, V0, n, 0, true);
    double dV_1 = this->dF_dV();
    this->parameters_TV(T0 + dX*T0, V0, n, 0, true);
    double dV1 = this->dF_dV();
    double dVdT_num = (dV1 - dV_1) / (2*dX*T0);
    d = std::log(std::fabs(dTdV + 1e-15)) - std::log(std::fabs(dVdT_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2F/dVdT != d2F/dVdT", std::vector<double>{dTdV, dVdT_num}); error_output++; }

    this->parameters_TV(T0, V0 - dX*V0, n, 0, true);
    double dT_1 = this->dF_dT();
    this->parameters_TV(T0, V0 + dX*V0, n, 0, true);
    double dT1 = this->dF_dT();
    double dTdV_num = (dT1 - dT_1) / (2*dX*V0);
    d = std::log(std::fabs(dTdV + 1e-15)) - std::log(std::fabs(dTdV_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2F/dTdV != d2F/dTdVdT", std::vector<double>{dTdV, dTdV_num}); error_output++; }

    // d2F/dV2
    this->parameters_TV(T0, V0 - V0*dX, n, 0, true);
    dV_1 = this->dF_dV();
    this->parameters_TV(T0, V0 + V0*dX, n, 0, true);
    dV1 = this->dF_dV();
    double dV2_num = (dV1 - dV_1) / (2*dX*V0);
    d = std::log(std::fabs(dV2 + 1e-15)) - std::log(std::fabs(dV2_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2F/dV2 != d2F/dV2", std::vector<double>{dV2, dV2_num, d}); error_output++; }

    // d2F/dT2
    this->parameters_TV(T0 - T0*dX, V0, n, 0, true);
    dT_1 = this->dF_dT();
    this->parameters_TV(T0 + T0*dX, V0, n, 0, true);
    dT1 = this->dF_dT();
    double dT2_num = (dT1 - dT_1) / (2*dX*T0);
    d = std::log(std::fabs(dT2 + 1e-15)) - std::log(std::fabs(dT2_num + 1e-15));
    if (std::fabs(d) > tol) { print("d2F/dT2 != d2F/dT2", std::vector<double>{dT2, dT2_num, d}); error_output++; }
    
    return error_output;
}

int HelmholtzEoS::lnphi_test(double p_, double T_, std::vector<double>& n, double tol) 
{
    // d/dnj (sum_i n_i lnphii) = G_rj/RT = lnphij
    int error_output = 0;

    // Calculate d(Gri/RT)/dnj numerically
    this->parameters(p_, T_, n, 0, false);
    std::vector<double> lnphi0 = this->lnphi();

    // at p, T, n
    double Gres0 = 0.;
    for (int i = 0; i < nc; i++) {
        Gres0 += n[i] * lnphi0[i];
    }

    // add dn
    double dn = 1e-8;
    std::vector<double> dGres(nc, 0.);
    for (int j = 0; j < nc; j++)
    {
        n[j] += dn;
        this->parameters(p_, T_, n, 0, false);
        std::vector<double> lnphi1 = this->lnphi();

        double Gres1 = 0.;
        for (int i = 0; i < nc; i++)
        {
            Gres1 += n[i] * lnphi1[i];
        }
        dGres[j] = (Gres1-Gres0)/dn;
        n[j] -= dn;
    }

    // compare lnphi's
    for (int j = 0; j < nc; j++)
    {
        double d = std::log(std::fabs(dGres[j] + 1e-15)) - std::log(std::fabs(lnphi0[j] + 1e-15));
        if (std::fabs(d) > tol)
        {
            print("comp", j);
            print("lnphi consistency test", std::vector<double>{dGres[j], lnphi0[j], d});
            error_output++;
        }
    }
    return error_output;
}

int HelmholtzEoS::pressure_test(double p_, double T_, std::vector<double>& n, double tol) 
{
    // Pressure consistency test
    // d/dP sum_i dlnphii/dP = (Z-1)N/P
    int error_output = 0;

    // Calculate dlnphi/dP from EoS
    this->parameters(p_, T_, n, 0, true);

    dlnphidP = this->dlnphi_dP();
    double ndlnphi_dp = std::inner_product(n.begin(), n.end(), dlnphidP.begin(), 0.);

    // compare dlnphi_dP with (Z-1)/P
    if (std::fabs(ndlnphi_dp - (this->z-1.)*N/p) > tol)
    {
        print("Pressure consistency test", std::vector<double>{ndlnphi_dp, (this->z-1.)/p});
        error_output++;
    }
    return error_output;
}

int HelmholtzEoS::temperature_test(double p_, double T_, std::vector<double>& n, double tol) 
{
    (void) p_;
    (void) T_;
    (void) n;
    (void) tol;
    return 0;
}

int HelmholtzEoS::composition_test(double p_, double T_, std::vector<double>& n, double tol) 
{
    // Consistency of composition derivatives: symmetry of dlnphii/dnj and Gibbs-Duhem relation
    int error_output = 0;

    // Calculate all dlnphii/dnj
    this->parameters(p_, T_, n, 0, true);
    dlnphidn = this->dlnphi_dn();

    // Symmetry of dlnphii_dnj: dlnphii/dnj == dlnphij/dni
    for (int i = 0; i < nc; i++)
    {
        for (int j = 0; j < nc; j++)
        {
            double d = (this->dlnphidn[i*nc+j]-this->dlnphidn[j*nc+i])/this->dlnphidn[i*nc+j];
            if (std::fabs(d) > tol)
            {
                print("symmetry", std::vector<double>{dlnphidn[i*nc+j], dlnphidn[j*nc+i], d});
                error_output++;
            }
        }
    }

    // Gibbs-Duhem relation
    // sum_i n_i dlnphii/dnj = 0
    for (int j = 0; j < nc; j++)
    {
        double G_D = 0;
        for (int i = 0; i < nc; i++)
        {
            G_D += n[i] * this->dlnphidn[i*nc+ j];
        }
        if (std::fabs(G_D) > tol)
        {
            print("Gibbs-Duhem", G_D);
            error_output++;
        }
    }
    return error_output;
}

int HelmholtzEoS::pvt_test(double p_, double T_, std::vector<double>& n, double tol)
{
    // Consistency of EoS PVT: Calculate volume roots at P, T, n and find P at roots (T, V, n)
    int error_output = 0;

    // Evaluate properties at P, T, n
    this->parameters(p_, T_, n, 0, true);
    
    for (std::complex<double> Z: this->Z_roots)
    {
        if (Z.imag() == 0.)
        {
            // Calculate total volume
            double V = Z.real() * N * this->units.R * T / p;

            // Evaluate P(T,V,n)
            double pp = this->P(T, V, n);
            if (std::fabs(pp - p) > tol)
            {
                print("P(T, V, n) != p", std::vector<double>{pp, p, std::fabs(pp-p)});
                error_output++;
            }
        }
    }

    return error_output;
}

int HelmholtzEoS::properties_test(double p_, double T_, std::vector<double>& n, double tol)
{
    // Consistency of EoS residual properties at (P, T, n)
    int error_output = 0;
    double p0 = p_;
    double T0 = T_;
    double V0 = this->V(p0, T0, n);

    // Gibbs free energy Gr_TP(P, T, n)
    double Gr = EoS::Gr_TP(p0, T0, n);
    double Grhh = this->Gr_TP(p0, T0, n);
    if (std::fabs(Gr - Grhh) > tol) { print("Gr != Gr", std::vector<double>{Gr, Grhh}); error_output++; }

    // Enthalpy Hr_TP(P, T, n)
    double Hr = EoS::Hr_TP(p0, T0, n);
    double Hrhh = this->Hr_TP(p0, T0, n);
    if (std::fabs(Hr - Hrhh) > tol) { print("Hr != Hr", std::vector<double>{Hr, Hrhh}); error_output++; }

    // Entropy Sr_TP(P, T, n)
    double Sr = EoS::Sr_TP(p0, T0, n);
    double Srhh = this->Sr_TP(p0, T0, n);
    if (std::fabs(Sr - Srhh) > tol) { print("Sr != Sr", std::vector<double>{Sr, Srhh}); error_output++; }

    // Helmholtz free energy Ar_TP(P, T, n)
    // double Ar = EoS::Ar_TP(p0, T0, n);
    // double Arhh = this->Ar_TP(p0, T0, n);
    // if (std::fabs(Ar - Arhh) > tol) { print("Ar != Ar", std::vector<double>{Ar, Arhh}); error_output++; }

    // Internal energy Ur_TP(P, T, n)
    // double Ur = EoS::Ur_TP(p0, T0, n);
    // double Urhh = this->Ur_TP(p0, T0, n);
    // if (std::fabs(Ur - Urhh) > tol) { print("Ur != Ur", std::vector<double>{Ur, Urhh}); error_output++; }

    // Thermal properties tests
    double dT = 1e-3;

    // Residual heat capacity at constant volume Cvr
    double CVr = this->Cvr(p0, T0, n, 0) * this->units.R;
    double Ur0 = this->Ur_TV(T0, V0, n);
    double Ur1 = this->Ur_TV(T0 + dT, V0, n);
    double CVr_num = (Ur1-Ur0)/dT;
    if (std::fabs(CVr - CVr_num) > tol) { print("Cvr != Cvr", std::vector<double>{CVr, CVr_num}); error_output++; }

    // // Heat capacity at constant volume Cv
    // double CV = this->Cv(p, T, n, 0) * this->units.R;
    // double U0 = this->U_TV(T0, V0, n);
    // double U1 = this->U_TV(T0+dT, V0, n);
    // double CV_num = (U1-U0)/dT;
    // if (std::fabs(CV - CV_num) > tol) { print("Cv != Cv", std::vector<double>{CV, CV_num}); error_output++; }

    // Residual heat capacity at constant pressure Cpr
    double CPr = this->Cpr(p0, T0, n, 0) * this->units.R;
    double Hr1 = this->Hr_TP(p0, T0+dT, n);
    double CPr_num = (Hr1-Hrhh)/dT;
    if (std::fabs(CPr - CPr_num) > tol) { print("Cpr != Cpr", std::vector<double>{CPr, CPr_num}); error_output++; }

    // Heat capacity at constant pressure Cp
    double CP = this->Cp(p0, T0, n, 0);
    double H0 = this->H_TP(p0, T0, n);
    double H1 = this->H_TP(p0, T0+dT, n);
    double CP_num = (H1-H0)/dT;
    if (std::fabs(CP - CP_num) > tol) { print("Cp != Cp", std::vector<double>{CP, CP_num}); error_output++; }

    return error_output;
}
