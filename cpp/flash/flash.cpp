#include <iostream>
#include <cmath>
#include <vector>
#include <numeric>
#include <algorithm>
#include <map>

#include "dartsflash/eos/trial_phase.hpp"
#include "dartsflash/flash/flash.hpp"
#include "dartsflash/flash/flash_params.hpp"
#include "dartsflash/stability/stability.hpp"
#include "dartsflash/phase-split/twophasesplit.hpp"
#include "dartsflash/phase-split/multiphasesplit.hpp"

Flash::Flash(FlashParams& flashparams, int np_max_) {
	this->flash_params = flashparams;

    this->nc = flash_params.nc;
	this->ni = flash_params.ni;
	this->ns = flash_params.ns;

    this->np_max = np_max_;

    this->z.resize(ns);
    this->eos.reserve(np_max);
    this->nu.reserve(np_max);
    this->X.reserve(np_max*ns);

    this->stationary_points.reserve(np_max);
    this->ref_compositions.reserve(np_max);
}

void Flash::init(double p, double T, std::vector<double>& z_)
{
    // Set iterations to zero
    total_ssi_flash_iter = total_ssi_stability_iter = total_newton_flash_iter = total_newton_stability_iter = 0;
    
    // Initialize EoS at p, T
    this->flash_params.init_eos(p, T);
    this->flash_params.initial_guess.init(p, T);

    // Check if feed composition needs to be corrected for 0 values
    z = std::vector<double>(ns);
    for (int i = 0; i < ns; i++)
    {
        z[i] = (z_[i] > flash_params.min_z) ? z_[i] : flash_params.min_z;
    }
    return;
}

int Flash::run_stability(std::vector<TrialPhase>& ref_comps)
{
    // Run stability test on feed X with initial guesses Y
    Stability stab(flash_params);
    stab.init(ref_comps[0]);

    // Add each of the minima trivial stationary points (for np > 1, this corresponds to all phase compositions)
    this->stationary_points.clear();
    if (ref_comps.size() > 1 || stab.is_minimum(ref_comps[0]))
    {
        this->stationary_points = ref_comps;
    }

    // Iterate over initial guesses in Y to run stability tests
    for (auto it: flash_params.eos_params)
    {
        std::vector<TrialPhase> trial_comps = flash_params.initial_guess.evaluate(it.first, it.second.initial_guesses, ref_comps);
        for (TrialPhase trial_comp: trial_comps)
        {
            int error = stab.run(trial_comp);

            if (error > 0)
            {
                return error;
            }

            // Get TPD value and check if it is already in the list
            if (!this->compare_stationary_points(trial_comp))
            {
                // No duplicate found: add stationary point to vector of stationary points
                stationary_points.push_back(trial_comp);
            }

            // Get number of iterations from stability
            this->total_ssi_stability_iter += stab.get_ssi_iter();
            this->total_newton_stability_iter += stab.get_newton_iter();
        }
    }
    return 0;
}

int Flash::run_split()
{
    // Choose proper set of lnK
    std::vector<double> lnK = this->generate_lnK();

    // Initialize split object
    int error;
    np = static_cast<int>(lnK.size()/ns) + 1;
    if (np == 2)
    {
        // Initialize PhaseSplit object for two phases
        TwoPhaseSplit split(flash_params);

        // Run split and return nu and x
        error = split.run(z, lnK, eos);  // Run multiphase split algorithm at P, T, z with initial guess lnK
        nu = split.getnu();
        X = split.getx();
        total_ssi_flash_iter += split.get_ssi_iter();
        total_newton_flash_iter += split.get_newton_iter();
    }
    else
    {
        // Initialize MultiPhaseSplit object for 3 or more phases
        MultiPhaseSplit split(flash_params, np);

        // Run split and return nu and x
        error = split.run(z, lnK, eos);  // Run multiphase split algorithm at P, T, z with initial guess lnK
        nu = split.getnu();
        X = split.getx();
        total_ssi_flash_iter += split.get_ssi_iter();
        total_newton_flash_iter += split.get_newton_iter();
    }

    // Determine output value
    if (error)
    {
        // Error occurred: return 1
        return 1;
    }
    else
    {
        // Check if all phases are positive
        // If so, return 0
        int output = 0;
        ref_compositions.clear();
        for (int j = 0; j < np; j++)
        {
            if (nu[j] < 0.)
            {
                // If at least one phase is negative, return -1
                output = -1;
            }
            
            std::vector<double> xj(ns);
		    auto start = X.begin() + j*ns;
		    std::copy(start, start + ns, xj.begin());

            ref_compositions.push_back(TrialPhase{eos[j], xj});
        }
        return output;
    }
}

int Flash::run_loop(std::vector<TrialPhase>& ref_comps)
{    
    // Perform stability test starting from each of the initial guesses
    int stability_output = this->run_stability(ref_comps);
    if (stability_output > 0)
    {
        // Error occurred in stability test
        return stability_output;
    }

    // Count number of stationary points with negative TPD
    int negative_tpds = 0;
    double tpd_min = -flash_params.tpd_tol;
    for (TrialPhase stationary_point : stationary_points)
    {
        if (stationary_point.tpd < -flash_params.tpd_tol && stationary_point.preferred)
        {
            negative_tpds++;
            if (stationary_point.tpd < tpd_min)
            {
                tpd_min = stationary_point.tpd;
            }
        }
    }

    // If no negative TPDs, feed is stable, return -1
    if (negative_tpds == 0)
    {
        return -1;
    }
    else
    {
        // Else not stable
        if (flash_params.verbose)
        {
            print("Unstable feed", "============");
            print("X", X, np);
            for (TrialPhase stationary_point : stationary_points) { stationary_point.print_point(); }
        }
        
        // With new K-values, run NP+1 phase split
        int split_output = this->run_split();
        
        if (split_output == 0)
        {
            // Split success
            return 0;
        }
        else
        {
            return 1;
        }
    }
}

std::vector<double> Flash::generate_lnK()
{
    // Determine lnK initialization for phase split
    std::vector<double> lnK;

    // Find <= zero TPDs
    std::vector<double> lnY0(nc);
    eos.clear();

    int k = 0;
    for (TrialPhase stationary_point : this->stationary_points)
    {
        if (stationary_point.tpd <= 0. && stationary_point.preferred)
        {
            eos.push_back(stationary_point.eos_name);

            if (k == 0)
            {
                for (int i = 0; i < nc; i++)
                {
                    lnY0[i] = std::log(stationary_point.Y[i]);
                }
            }
            else
            {
                lnK.resize(k*nc);
                for (int i = 0; i < nc; i++)
                {
                    lnK[(k-1)*nc + i] = std::log(stationary_point.Y[i]) - lnY0[i];
                }
            }
            k++;
        }
    }

    return lnK;
}

bool Flash::compare_stationary_points(TrialPhase& stationary_point)
{
    // Compare stationary point with entries in vector of stationary points to check if it is unique
    // Returns true if point is already in the list
    double tpd0 = stationary_point.tpd;
    double lntpd0 = std::log(std::fabs(tpd0));
    for (size_t j = 0; j < stationary_points.size(); j++)
    {
        double tpdj = stationary_points[j].tpd;
        // For small tpd difference (tpd < 1), compare absolute difference; for large tpd values, logarithmic scale is used to compare
        double tpd_diff = lntpd0 < 0. ? std::fabs(tpdj-tpd0) : lntpd0 - std::log(std::fabs(tpdj) + 1e-15);
        if (stationary_points[j].eos_name == stationary_point.eos_name // eos is the same
            && (tpd_diff < flash_params.tpd_tol)) // tpd is within tolerance
        {
            // Similar TPD found; Check if composition is also the same
            if (compare_compositions(stationary_point.Y, stationary_points[j].Y, flash_params.comp_tol))
            {
                return true;
            }
        }
    }

    // Find if there is a preferred EoS in this range
    if (!flash_params.preferred_eos.empty())
    {
        int max_idx = std::distance(stationary_point.Y.begin(), std::max_element(stationary_point.Y.begin(), stationary_point.Y.end()));
        if (stationary_point.Y[max_idx] >= flash_params.y_pure // we are in almost "pure phase" range of composition
                && flash_params.preferred_eos.count(max_idx) > 0  // preferred EoS entry for max_comp key exists
                && flash_params.preferred_eos[max_idx] != stationary_point.eos_name) // eos is not preferred EoS
        {
            stationary_point.preferred = false;
        }
    }

    return false;
}
