#include <iostream>
#include <cmath>
#include <vector>
#include <Eigen/Dense>

#include "dartsflash/global/timer.hpp"
#include "dartsflash/flash/analysis.hpp"
#include "dartsflash/flash/flash.hpp"
#include "dartsflash/stability/stability.hpp"

Analysis::Analysis(FlashParams& flashparams) {
	this->flash_params = flashparams;

    this->nc = flash_params.nc;
	this->ni = flash_params.ni;
	this->ns = flash_params.ns;

    h.resize(ns);
}

void Analysis::init_stability(double p, double T, std::vector<double>& x, std::string ref_eos)
{
    // Initialise stability algorithm for p, T, x
    this->flash_params.init_eos(p, T);

    // Call Stability.init(ref_comp) method
    Stability stab(flash_params);
    ref_comp = TrialPhase(ref_eos, x);
    stab.init(ref_comp);
    this->h = stab.h;
    return;
}

std::vector<std::vector<double>> Analysis::calc_stability_path(std::vector<double>& Y, std::string eosname)
{
    Stability stab(flash_params);
    stab.init(this->ref_comp);

    trial_comp = TrialPhase(eosname, Y);
    stab.run(trial_comp);

    return stab.get_solution_path();
}

std::vector<TrialPhase> Analysis::find_stationary_points(double p, double T, std::vector<double>& X, std::vector<std::string>& eos_names)
{
    // Initialize EoS and InitialGuess at p, T
    this->flash_params.init_eos(p, T);
    // this->flash_params.initial_guess.set_initial_guesses(flash_params.stability_initial_guesses);
    this->flash_params.initial_guess.init(p, T);
    
    // Perform stability test starting from each of the initial guesses
    std::vector<TrialPhase> ref_comps{};
    for (size_t j = 0; j < eos_names.size(); j++)
    {
        std::vector<double> x(nc);
        auto start = X.begin() + j*nc;
        std::copy(start, start + nc, x.begin());
        ref_comps.push_back(TrialPhase(eos_names[j], x));
    }

    //
    StabilityFlash flash(flash_params);
    this->stationary_points.clear();
    int stability_output = flash.run_stability(ref_comps);
    if (stability_output > 0 && flash_params.verbose)
    {
        // Error occurred in stability test
		print("ERROR in find_stationary_points()", stability_output);
		print("p", p, 10);
        print("T", T, 10);
        for (TrialPhase ref: ref_comps)
        {
            ref.print_point();
        }
        return ref_comps;
    }
    return flash.stationary_points;
}
