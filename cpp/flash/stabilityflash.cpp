#include <iostream>
#include <cmath>
#include <algorithm>
#include <vector>
#include <numeric>

#include "dartsflash/flash/flash.hpp"
#include "dartsflash/stability/stability.hpp"

StabilityFlash::StabilityFlash(FlashParams& flashparams, int np_max_) : Flash(flashparams, np_max_) { }

int StabilityFlash::evaluate(double p, double T, std::vector<double>& z_)
{
    // Evaluate sequential stability + flash algorithm
    
    // Initialize flash
    // Initialize EoS at p, T and check if feed composition needs to be corrected
    this->init(p, T, z_);
    
    // Find reference compositions - hypothetical single phase
    double gmin = NAN;
    std::string ref_eos = "";
    for (auto it: flash_params.eos_map)
    {
        if (it.second->eos_in_range(z.begin()))
        {
            double g = it.second->Gr_TP(p, T, z_);

            if (!flash_params.preferred_eos.empty())
            {
                int max_comp = std::distance(z.begin(), std::max_element(z.begin(), z.end()));

                if (z[max_comp] >= flash_params.y_pure // make sure we are in "pure phase" range of composition
                        && flash_params.preferred_eos.count(max_comp) > 0  // preferred EoS entry for max_comp key exists
                        && it.first == flash_params.preferred_eos[max_comp]) // EoS is preferred EoS for this component
                {
                    ref_eos = it.first;
                    break;
                }
            }

            if (std::isnan(gmin) // gmin not initialized
                        || (g < gmin)) // Gibbs energy of EoS is lower
            {
                ref_eos = it.first;
                gmin = g;
            }
        }
    }
    ref_compositions = {TrialPhase(ref_eos, z)};
    eos = {ref_eos};

    // Perform stability and phase split loop starting from np = 1
    this->np = 1;
    this->nu = {1.};
    this->X = z;

    // Run stability test + flash loop over phases until np_max has been reached
    int it = 1;
    while (np < np_max)
    {
        int output = this->run_loop(ref_compositions);
        if (output == -1)
        {
            // Output -1, all phases stable
            if (flash_params.verbose)
            {
                print("StabilityFlash", "===============");
                print("p, T", std::vector<double>{p, T});
			    print("z", z_);
                print("nu", nu);
                print("X", X, np);
            }
            return 0;
        }
        else if (output > 0 || it > 10)
        {
            // Else, error occurred in split
            if (flash_params.verbose)
            {
                print("ERROR in StabilityFlash", output);
    		    print("p, T", std::vector<double>{p, T}, 10);
			    print("z", z_, 10);
            }
            this->nu = std::vector<double>(np, NAN);
		    this->X = std::vector<double>(np*ns, NAN);
            return output;
        }
        it++;
    }

    return 0;
}
