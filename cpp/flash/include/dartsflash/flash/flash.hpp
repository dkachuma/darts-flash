//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_FLASH_FLASH_H
#define OPENDARTS_FLASH_FLASH_FLASH_H
//--------------------------------------------------------------------------

#include <vector>
#include "dartsflash/global/global.hpp"
#include "dartsflash/eos/trial_phase.hpp"
#include "dartsflash/flash/flash_params.hpp"
#include "dartsflash/flash/analysis.hpp"

class Flash
{
protected:
    int nc, ni, ns, np, np_max;
	int total_ssi_flash_iter, total_ssi_stability_iter, total_newton_flash_iter, total_newton_stability_iter;
	std::vector<double> z, nu, X;
	std::vector<std::string> eos;
	std::vector<TrialPhase> ref_compositions, stationary_points;
	FlashParams flash_params;

public:
	Flash(FlashParams& flashparams, int np_max_ = 6);
	virtual ~Flash() = default;

	virtual int evaluate(double p, double T, std::vector<double>& z_) = 0;

	friend class Analysis;

protected:
	void init(double p, double T, std::vector<double>& z_);
	
	int run_stability(std::vector<TrialPhase>& ref_comps);
	int run_split();
	int run_loop(std::vector<TrialPhase>& ref_comps);

	virtual std::vector<double> generate_lnK();
	bool compare_stationary_points(TrialPhase& stationary_point);

public:
	struct Results
	{
		std::vector<double> nu, X;
		std::vector<std::string> eos;

		Results(std::vector<double>& nu_, std::vector<double>& X_, std::vector<std::string>& eos_)
		: nu(nu_), X(X_), eos(eos_) { }

		const std::vector<double> &get_phase_fractions() const { return this->nu; }
		const std::vector<double> &get_phase_compositions() const { return this->X; }
		const std::vector<std::string> &get_eos() const { return this->eos; }
	};
	Results get_flash_results() { return Results(this->nu, this->X, this->eos); }
	const std::vector<double>& getnu() const { return this->nu; }
	const std::vector<double>& getx() const { return this->X; }

    int get_flash_total_ssi_iter(){return total_ssi_flash_iter;}
    int get_flash_total_newton_iter(){return total_newton_flash_iter;}
    int get_stability_total_ssi_iter(){return total_ssi_stability_iter;}
    int get_stability_total_newton_iter(){return total_ssi_stability_iter;}
};

class StabilityFlash : public Flash
{
public:
	StabilityFlash(FlashParams& flashparams, int np_max_ = 6);

	virtual int evaluate(double p, double T, std::vector<double>& z_) override;

};

class NegativeFlash : public Flash
{
protected:
	std::vector<int> initial_guesses;

public:
	NegativeFlash(FlashParams& flashparams, const std::vector<std::string>& eos_used, const std::vector<int>& initial_guesses_);

	virtual int evaluate(double p, double T, std::vector<double>& z_) override;

protected:
	virtual std::vector<double> generate_lnK() override;
	virtual bool check_negative();
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_FLASH_FLASH_H
//--------------------------------------------------------------------------
