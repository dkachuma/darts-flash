//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_FLASH_PHFLASH_H
#define OPENDARTS_FLASH_FLASH_PHFLASH_H
//--------------------------------------------------------------------------

#include <vector>
#include "dartsflash/global/global.hpp"
#include "dartsflash/eos/trial_phase.hpp"
#include "dartsflash/flash/flash_params.hpp"
#include "dartsflash/flash/flash.hpp"

class PhFlash
{
protected:
    int nc, ni, ns, np, np_max;
	int total_ssi_flash_iter, total_ssi_stability_iter, total_newton_flash_iter, total_newton_stability_iter;
	int error = 0;
	double T_out, h_spec, p;

	double T_max, T_min, T_init;
	double tol;
	
	std::vector<double> z, nu, X;
	std::vector<std::vector<double>> X2;
	std::vector<std::string> eos;

	FlashParams flash_params;

public:
	PhFlash(FlashParams& flashparams, int np_max_ = 6);
	PhFlash();
	~PhFlash();

	int evaluate(double p_, double h_spec_, std::vector<double>& z_);

protected:
	void init(double p_, double h_spec_, std::vector<double>& z_);
	double brent_method(double a, double b, double t, double tol_f, double tol_t, int printIter_flag);
	double fun(double T);

public:
	std::vector<double> getnu() { return this->nu; }
	std::vector<double> getx() { return this->X; }
	double getT() { return T_out; }
	double getH(double T_test) { return fun(T_test); }

};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_FLASH_PHFLASH_H
//--------------------------------------------------------------------------
