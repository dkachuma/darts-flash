#include <cmath>
#include "dartsflash/global/timer.hpp"
#include "dartsflash/flash/flash_params.hpp"

FlashParams::FlashParams()
{
	for (int i = 0; i < timer::TOTAL; i++)
	{
		timers[i] = new Timer();
	}
}

FlashParams::FlashParams(CompData& compdata) : FlashParams()
{
	this->comp_data = compdata;
	this->nc = compdata.nc;
	this->ni = compdata.ni;
	this->ns = nc + ni;

	this->initial_guess = InitialGuess(compdata);
}

void FlashParams::add_eos(std::string name, EoS* eos)
{
	eos_map[name] = eos->getCopy();
	eos_params[name] = EoSParams();
	return;
}

void FlashParams::init_eos(double p, double T)
{
	// Initialise EoS component parameters at p, T
    this->start_timer(timer::EOS);
	for (auto& it: this->eos_map) {
		it.second->parameters(p, T);
	}
    this->stop_timer(timer::EOS);
}

void FlashParams::init_eos(double p, double T, std::vector<double>& n, int start_idx, bool second_order)
{
	// Initialise EoS component parameters at p, T, n
    this->start_timer(timer::EOS);
	for (auto& it: this->eos_map) {
		it.second->parameters(p, T, n, start_idx, second_order);
	}
    this->stop_timer(timer::EOS);
}

void FlashParams::print_timers()
{
	for (size_t i = 0; i < timers.size(); i++)
	{
		print("time", timers[i]->elapsedMicroseconds());
		std::cout << timer_names[i] << ": " << timers[i]->elapsedMicroseconds() << " microseconds\n";
	}
	return;
}
