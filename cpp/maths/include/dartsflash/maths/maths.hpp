//--------------------------------------------------------------------------
#ifndef OPENDARTS_FLASH_MATHS_MATHS_H
#define OPENDARTS_FLASH_MATHS_MATHS_H
//--------------------------------------------------------------------------

#include <vector>
#include <complex>

int factorial(int n);

std::vector<std::complex<double>> cubic_roots_analytical(double a2, double a1, double a0);
std::vector<std::complex<double>> cubic_roots_iterative(double a2, double a1, double a0, double tol, int max_iter=50);

// Combinations and factorial used in np negative flash
class Combinations {
private:
    std::vector<int> m_arr;
    std::vector<int> m_data;
    int Ne; // number of unique elements
    int L; // length of combinations
    int index0 = 0;
public:
    Combinations(int ni, int ne, int l);
    int getIndex(int i, int k) { return m_data[i*L + k]; }
private:
    void unique_combinations(std::vector<int> temp, int start, int end, int index);
};

//--------------------------------------------------------------------------
#endif // OPENDARTS_FLASH_MATHS_MATHS_H
//--------------------------------------------------------------------------
