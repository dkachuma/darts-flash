import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import os

from dartsflash.diagram.analysis import Analysis
from dartsflash.libflash import InitialGuess, FlashParams
from dartsflash.mixtures.mixtures import Mixture, Y8


if 1:
    from dartsflash.mixtures.mixtures import Mixture
    from dartsflash.libflash import CubicEoS, AQEoS
    mix = Mixture(components=["H2O", "CO2", "C1"], name="H2O-CO2-C1", np_max=2)
    a = Analysis(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])
    a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                          AQEoS.solute: AQEoS.Ziabakhsh2012}))
    a.init_flash(stabilityflash=False, eos=["AQ", "PR"], initial_guess=[InitialGuess.Henry_AV])

    state_spec = ["pressure", "temperature"]
    zrange = np.concatenate([np.array([1e-14, 1e-12, 1e-10, 1e-8]),
                             np.linspace(1e-6, 1. - 1e-6, 10),
                             np.array([1. - 1e-8, 1. - 1e-10, 1. - 1e-12, 1. - 1e-14])])
    dimensions = {"pressure": np.array([1., 2., 5., 10., 50., 100., 200., 400.]),
                  "temperature": np.arange(273.15, 373.15, 10),
                  "H2O": zrange,
                  "CO2": zrange,
                  }
    constants = {"C1": 1.}
    dims_order = ["H2O",
                  "CO2",
                  "pressure",
                  "temperature"]
    fname = mix.name

    ref = xr.open_dataset("../../tests/python/data/ref_flash_brine_vapour2.nc")

    for i, t in enumerate(dimensions['temperature']):
        for j, p in enumerate(dimensions['pressure']):
            for k, zc1 in enumerate(dimensions['CO2']):
                for m, zh2o in enumerate(dimensions['H2O']):
                    refdata = ref.isel(pressure=j, temperature=i, H2O=m, CO2=k)
                    state_ref = [ref.pressure[j].values, ref.temperature[i].values, ref.H2O[m].values, ref.CO2[k].values, 1.-ref.H2O[m].values-ref.CO2[k].values]
                    state = [p, t, zh2o, zc1, 1.-zh2o-zc1]
                    # print(state)
                    if state[-1] > 1e-15:
                        res = a.evaluate_single_flash(state=state)
                        # print(res['X'])
                        if not np.allclose(res['nu'], refdata.nu.values[:2], rtol=1e-10):
                            print(state)
                            print("nu", res['nu'])
                            print("nuref", refdata.nu.values[:2])
                        elif not np.allclose(res['X'], refdata.X.values[:6], rtol=1e-10):
                            print(state, state_ref)
                            print("x", res['X'])
                            print("xref", refdata.X.values[:6])

    results = a.evaluate_flash(state_spec=state_spec, dimensions=dimensions, constants=constants,
                               mole_fractions=True, dims_order=dims_order)

elif 0:
    from dartsflash.mixtures.mixtures import Y8
    from dartsflash.libflash import CubicEoS
    mix = Y8()
    a = Analysis(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson])

    state_spec = ["pressure", "temperature"]
    components = mix.comp_data.components[:-1]
    dimensions = {"pressure": np.arange(10., 250., 5.),
                  "temperature": np.arange(190., 450., 10.),
                  }
    constants = {component: mix.composition[i] for i, component in enumerate(mix.comp_data.components[:-1])}
    dims_order = [
                  "pressure",
                  "temperature"]
    fname = "Y8"

elif 1:
    from dartsflash.mixtures.mixtures import MaljamarSep
    from dartsflash.libflash import CubicEoS
    mix = MaljamarSep()
    a = Analysis(mixture=mix)

    a.add_eos("PR", CubicEoS(mix.comp_data, CubicEoS.PR),
              initial_guesses=[InitialGuess.Yi.Wilson, InitialGuess.Yi.Wilson13, 0], switch_tol=1e-2)
    a.flash_params.stability_variables = FlashParams.alpha
    a.flash_params.split_variables = FlashParams.nik
    a.flash_params.split_switch_tol = 1e-5
    # a.flash_params.verbose = True

    state_spec = ["pressure", "temperature"]
    dimensions = {"pressure": np.arange(64., 80., 0.1),
                  # "temperature": np.arange(190., 450., 10.),
                  "CO2": np.arange(0.65, 1., 0.002),
                  }
    constants = {component: mix.composition[i+1] for i, component in enumerate(mix.comp_data.components[1:])}
    constants.update({"temperature": 305.35})
    dims_order = ["CO2",
                  "pressure"]
    fname = mix.name

    a.init_flash()

    # flash_output = a.evaluate_single_flash(state=[68.5, 305.35, 0.9, 0.02354, 0.03295, 0.01713, 0.01099, 0.00574, 0.00965])
    # flash_output = a.evaluate_single_flash(state=[64, 305.35, 0.68, 0.075328, 0.10544, 0.054816, 0.035168, 0.018368, 0.03088])
    flash_output = a.evaluate_single_flash(state=[64, 305.35, 0.99, 0.002354, 0.003295, 0.001713, 0.001099, 0.000574, 0.000965])

    # print(flash_output)

    flash_data: xr.DataArray
    flash_data = a.evaluate_flash(state_spec, dimensions, constants, mole_fractions=True, dims_order=dims_order)
    print(flash_data)
    flash_data.np.plot()
    print(flash_data.nu[:, :, 0])

# properties = ["Gr_TP"]
# prop_data = a.evaluate_residual_properties_1p(state_spec, components, dimensions, constants, properties, eos="PR", mole_fractions=True,
#                                               dims_order=dims_order, fname='properties')
# print(prop_data)
# prop_data.Gr_TP_PR.plot()

plt.show()

