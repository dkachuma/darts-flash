import numpy as np
import xarray as xr
import matplotlib.pyplot as plt
import os

from dartsflash.diagram.analysis import Analysis
from dartsflash.libflash import InitialGuess


from dartsflash.mixtures.mixtures import Mixture
from dartsflash.libflash import CubicEoS, AQEoS
mix = Mixture(components=["H2O", "CO2"], name="H2O-CO2", np_max=2)
a = Analysis(mixture=mix)

a.add_eos("CEOS", CubicEoS(mix.comp_data, CubicEoS.PR),
          initial_guesses=[InitialGuess.Yi.Wilson])
a.add_eos("AQ", AQEoS(mix.comp_data, {AQEoS.water: AQEoS.Jager2003,
                                      AQEoS.solute: AQEoS.Ziabakhsh2012}),
          eos_range={0: [0.9, 1.]})
a.init_flash(stabilityflash=False, eos=["AQ", "CEOS"], initial_guess=[InitialGuess.Ki.Henry_AV])

aq = a.flash_params.eos_map["AQ"]
ceos = a.flash_params.eos_map["CEOS"]

state_spec = ["pressure", "temperature"]
components = mix.comp_data.components[:-1]
dimensions = {"pressure": np.arange(1, 100., 1.),
              "temperature": np.arange(273.15, 523.15, 50.),
              "H2O": np.arange(0.001, 1., 0.001)}
constants = {}
dims_order = ["H2O",
              "pressure",
              "temperature"]

pressure = dimensions['pressure']
temperature = np.empty(np.shape(pressure))
temperature = dimensions['temperature']
nroots = np.empty((len(temperature), len(pressure)))

pressure, volume = a.evaluate_pressure_volume(eos_name="CEOS", p_min=pressure[5], p_max=pressure[-1], temperature=temperature,
                                              composition=[1, 0.], number_of_curves=len(temperature), nv=200)

from dartsflash.diagram.diagram import Plot
pl = Plot()
pl.draw_plot(volume, pressure, number_of_curves=len(temperature), #xlim=[0., 0.005], ylim=[0., 100.],
             xlabel="Molar volume [m3/mol]", ylabel="Pressure [bar]", title="P-V diagram H2O-CO2",
             datalabels=["T = {} K".format(temp) for temp in temperature])

zH2O = dimensions['H2O']
Ga, Gl, Gv = np.zeros(np.shape(zH2O)), np.zeros(np.shape(zH2O)), np.zeros(np.shape(zH2O))

pres = 20.
temp = 425.15
for i, z in enumerate(zH2O):
    Z = [z, 1-z]

    Ga[i] = aq.G_TP(pres, temp, Z)

    # Liquid root
    ceos.set_root_flag(0)
    Gl[i] = ceos.G_TP(pres, temp, Z)
    print("Z", ceos.Z())

    # Vapour root
    ceos.set_root_flag(1)
    Gv[i] = ceos.G_TP(pres, temp, Z)
    print("Gl/v i", Gl[i], Gv[i])

gibbs_plot = Plot()
gibbs_plot.draw_plot([zH2O, zH2O, zH2O], [Ga, Gl, Gv], number_of_curves=3,
                     title="Gibbs free energy of H2O-CO2 mixture", xlabel="xH2O [-]", ylabel="G/RT",
                     datalabels=["Aq", "L", "V"])

tmin = 370.15
R = mix.comp_data.units.R

# for i, p in enumerate(pressure):
# #     for j, t in enumerate(temperature):
#     ceos.parameters(p, t, [1., 0.])
#     Z = np.array(ceos.Zroots())
#     nroots[j, i] = np.sum(np.isreal(Z))
#
#     while True:
#         gA = aq.Gr_TP(p, temp, [1., 0.])
#
#         G = ceos.Gr_TP(p, temp, [1., 0.])
#
#         Z = np.array(ceos.Zroots())
#         Zv = np.max(Z[np.isreal(Z)].real)
#         Zl = np.min(Z[np.isreal(Z)].real)
#
#         Vv = Zv * R * temp / p
#         Vl = Zl * R * temp / p
#
#         ceos.parameters_TV(temp, Vv, [1., 0.])
#         gv = ceos.Gr_TP()
#
#         ceos.parameters_TV(temp, Vl, [1., 0.])
#         gl = ceos.Gr_TP()
#
#         if abs(gv > gl) > 1e-10:
#             break
#         elif gv > gl:
#             temp -=
#         #
#         print(Vv, Vl)
#         print(G, gv, gl)

        # break

from dartsflash.diagram.diagram import PhaseDiagram

# pl = Plot()
# pl.draw_plot(xdata=volume[:][0], ydata=volume[:][1], number_of_curves=1)

# pd = PhaseDiagram()
# pd.PT(pressure=pressure, temperature=temperature, data=nroots)

plt.show()
